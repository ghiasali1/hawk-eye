import React from 'react';
import Header from '../../components/header/Header';
import Map from '../../components/map/Map';

function HeaderMap(props) {
  return (
    <div>
      <Header user={props.user} />
      <Map panelid={props.panelid} />
    </div>
  );
}

export default HeaderMap;
