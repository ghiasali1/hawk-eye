import React from 'react';
import Header2 from '../../components/header/Header2';
import Map2 from '../../components/map/Map2';

function HeaderMap(props) {
  return (
    <div>
      <Header2 user={props.user} />
      <Map2 panelid={props.panelid} />
    </div>
  );
}

export default HeaderMap;
