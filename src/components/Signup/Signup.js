import React, { useState } from 'react';
import { Link, Redirect } from 'react-router-dom';
import './style.css';
import logo from '../../images/logo.png';
import { client } from '../../config/index';

export default function Signup() {
  const [email, setemail] = useState('');
  const [password, setpassword] = useState('');
  const [userName, setuserName] = useState('');
  const [loginS, setloginS] = useState(false);

  const onSubmit = () => {
    client
      .service('users')
      .create({
        strategy: 'local',
        email: email,
        password: password,
        userName: userName,
      })
      .then((res) => {
        setloginS(true);
      })
      .catch((e) => {
        console.log('ass');
        console.error('Authentication error', e);
      });
  };

  if (loginS) {
    return <Redirect to={{ pathname: '/' }} />;
  }
  return (
    <div style={{ fontFamily: 'poppins' }} className='container'>
      <div className='box'>
        <div className='box-1' style={{ elevation: 5 }}>
          <div className='color1122'>
            <img className='imagetag' src={logo} />{' '}
          </div>
          <input
            type='text'
            value={email}
            onChange={(e) => setemail(e.target.value)}
            name=''
            placeholder='Email'
          ></input>
          <input
            type='text'
            value={userName}
            onChange={(e) => setuserName(e.target.value)}
            name=''
            placeholder='User Name'
          ></input>
          <input
            type='Password'
            value={password}
            onChange={(e) => setpassword(e.target.value)}
            name=''
            placeholder='Password'
          ></input>
          <input
            type='submit'
            onClick={onSubmit}
            name=''
            value='SIGN UP'
          ></input>
          <div style={{ marginTop: '8%' }}>
            <p className='dont'>Already have Account? </p>
            <Link to='/'>
              {' '}
              <p className='sign'>SIGN In NOW</p>
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
}
