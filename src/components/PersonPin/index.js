import React, { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import { useLazyTranslate } from 'react-google-translate';
function Person() {
  const language = useSelector((state) => state.authReducer.setLanguage);
  const [text] = useState(['Turn On the Features Layer']);

  const [translate, { data, loading }] = useLazyTranslate({
    language,
  });

  useEffect(() => {
    if (text) {
      translate(text, language);
    }
  }, [translate, text, language]);
  return (
    <div
      style={{
        height: '89vh',
        backgroundColor: 'white',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
      }}
    >
      <div>
        <p style={{ fontFamily: 'poppins' }}>
          {loading ? 'Loading...' : data[0]}
        </p>
      </div>
    </div>
  );
}

export default Person;
