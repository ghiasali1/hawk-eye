import React from 'react';
import { Modal, Row, Col } from 'antd';
import Upl from '../../images/videos/movie.mp4';
import ReactPlayer from 'react-player';
import Map from './map';
function VideoModal(props) {
  return (
    <div>
      <Modal
        title='N1234K - MOI - Green'
        visible={props.visible}
        onCancel={props.onCancel}
        centered
        className='modal-body221'
        footer={[]}
      >
        <Row>
          <Col span={16}>
            <ReactPlayer
              // height={'70%'}
              // width={'100%'}
              playing={true}
              loop={true}
              controls={true}
              url={Upl}
            />
          </Col>
          <Col span={7}>
            <Map />
          </Col>
        </Row>
      </Modal>
    </div>
  );
}

export default VideoModal;
