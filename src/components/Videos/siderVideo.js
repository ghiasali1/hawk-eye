import React, { useState, useEffect } from 'react';
import './style.css';
import { Row, Col } from 'antd';
import Down from '../../images/videos/down.mp4';
import Upl from '../../images/videos/movie.mp4';
import ReactPlayer from 'react-player';
import VideoModal from './VideoModal';
import { useSelector } from 'react-redux';
import { useLazyTranslate } from 'react-google-translate';
function SiderVideo() {
  const [isModalVisible, setIsModalVisible] = useState(false);
  const language = useSelector((state) => state.authReducer.setLanguage);
  const [text] = useState(['Drone View']);

  const [translate, { data, loading }] = useLazyTranslate({
    language,
  });

  useEffect(() => {
    if (text) {
      translate(text, language);
    }
  }, [translate, text, language]);
  const showModal = () => {
    setIsModalVisible(true);
  };
  const handleCancel = () => {
    setIsModalVisible(false);
  };
  return (
    <div className='formDivasas'>
      <Col>
        <Row style={{ marginTop: '1rem' }} justify='center'>
          <h2 className='boxtext1'>{loading ? 'Loading...' : data[0]}</h2>
        </Row>
        <div id='topdiv' className='onhoverccc' onClick={showModal}>
          <Row justify='center'>
            <ReactPlayer
              height={'90%'}
              width={'90%'}
              playing={true}
              loop={true}
              url={Upl}
            />
          </Row>
          <Row justify='center'>
            <div className='nechywalabox'>
              <h3 className='boxtext'>N1234K - MOI - Green</h3>
            </div>
          </Row>
        </div>
        <div id='seconddiv' className='onhoverccc' onClick={showModal}>
          <Row style={{ marginTop: '3rem' }} justify='center'>
            <ReactPlayer
              height={'90%'}
              width={'90%'}
              playing={true}
              loop={true}
              url={Down}
            />{' '}
          </Row>
          <Row justify='center'>
            <div className='nechywalabox'>
              <h3 className='boxtext'>N1235K - MOI - Red</h3>
            </div>
          </Row>
        </div>
      </Col>
      <VideoModal visible={isModalVisible} onCancel={handleCancel} />
    </div>
  );
}

export default SiderVideo;
