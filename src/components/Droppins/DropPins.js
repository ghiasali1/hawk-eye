import React, { useState, useEffect } from 'react';
import './style.css';
import { Form, Button, Select, Input, message } from 'antd';
import { useSelector, useDispatch } from 'react-redux';
import { client } from '../../config/index';
import { setPinSelected } from '../../redux/actions';
import { useLazyTranslate } from 'react-google-translate';

const { Option } = Select;

function DropPins() {
  const dispatch = useDispatch();

  const pinSelected = useSelector((state) => state.authReducer.pinSelected);
  const latlang = useSelector((state) => state.authReducer.lnglat);
  const language = useSelector((state) => state.authReducer.setLanguage);
  const [text] = useState([
    'Please Click on the Map to drop pin',
    'Make sure you enable pins layers',
    'Observation Type',
    'Staging Area',
    'Drug House',
    'Area of intrest',
    'Illegal Crossing',
    'Political Distubrance',
    'Link Video',
    'No Link',
    'Description',
    'Submit',
  ]);

  const [translate, { data, loading }] = useLazyTranslate({
    language,
  });

  useEffect(() => {
    if (text) {
      translate(text, language);
    }
  }, [translate, text, language]);
  const onFinish = (values) => {
    client
      .service('pins')
      .create({
        point_type: values.observation,
        point_party: values.link,
        point_description: values.description,
        latlng: latlang,
      })
      .then((res) => {
        console.log(res);
        dispatch(setPinSelected(false));
        message.success('Pin Created Successfully');
      })
      .catch((e) => {
        // Show login page (potentially with `e.message`)
        console.error('Authentication error', e);
      });
  };

  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };
  function handleChange(value) {
    console.log(`selected ${value}`);
  }

  return (
    <div className='formDiv1122'>
      {pinSelected ? (
        <div>
          {' '}
          <Form
            name='basic'
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            autoComplete='off'
          >
            <p className='fontFamilys'>
              <strong>{loading ? 'Loading...' : data[2]}</strong>
            </p>

            <Form.Item
              name='observation'
              rules={[
                {
                  required: true,
                  message: 'Please input your observation!',
                },
              ]}
            >
              <Select
                defaultValue=''
                style={{ width: 200 }}
                onChange={handleChange}
              >
                <Option value='Staging Area'>
                  {loading ? 'Loading...' : data[3]}
                </Option>
                <Option value='Drug House'>
                  {loading ? 'Loading...' : data[4]}
                </Option>
                <Option value='Area of intrest'>
                  {loading ? 'Loading...' : data[5]}
                </Option>
                <Option value='Illegal Crossing'>
                  {loading ? 'Loading...' : data[6]}
                </Option>
                <Option value='Political Distubrance'>
                  {loading ? 'Loading...' : data[7]}
                </Option>
              </Select>
            </Form.Item>
            <p className='fontFamilys'>
              <strong>{loading ? 'Loading...' : data[8]}</strong>
            </p>
            <Form.Item
              name='link'
              rules={[
                {
                  required: false,
                },
              ]}
            >
              <Select
                defaultValue='No Link'
                style={{ width: 200 }}
                onChange={handleChange}
              >
                <Option value='No Link'>
                  {loading ? 'Loading...' : data[9]}
                </Option>
                <Option value='N1234K'>N1234K</Option>
                <Option value='N1235k'>N1235k</Option>
                <Option value='N3132K'>N3132K</Option>
              </Select>
            </Form.Item>
            <p className='fontFamilys'>
              <strong>{loading ? 'Loading...' : data[10]}</strong>
            </p>
            <Form.Item
              name='description'
              rules={[
                {
                  required: true,
                  message: 'Please input your description!',
                },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item>
              <Button
                type='primary'
                htmlType='submit'
                style={{
                  backgroundColor: 'rgb(246,193,68)',
                  border: 'solid white 1px',
                }}
              >
                {loading ? 'Loading...' : data[11]}
              </Button>
            </Form.Item>
          </Form>
        </div>
      ) : (
        <div className='lineheight'>
          <p className='fontFamilys'>{loading ? 'Loading...' : data[0]}</p>
          <p className='fontFamilys'>{loading ? 'Loading...' : data[1]}</p>
        </div>
      )}
    </div>
  );
}

export default DropPins;
