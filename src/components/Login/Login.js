import React, { useState } from 'react';
import { Link, Redirect } from 'react-router-dom';
import './style.css';
import logo from '../../images/logo.png';
import { client } from '../../config/index';
import { useDispatch, useSelector } from 'react-redux';
import { setUserProfile, setLoginState } from '../../redux/actions';
export default function Login() {
  const dispatch = useDispatch();

  const isloggedin = useSelector((state) => state.authReducer.isLoggedIn);

  const [email, setemail] = useState('');
  const [password, setpassword] = useState('');
  const [loginS, setloginS] = useState(false);

  const onSubmit = () => {
    client
      .authenticate({
        strategy: 'local',
        email: email,
        password: password,
      })
      .then((res) => {
        console.log(res);
        dispatch(setUserProfile(res.user));
        dispatch(setLoginState(true));
        setloginS(true);
      })
      .catch((e) => {
        // Show login page (potentially with `e.message`)
        console.error('Authentication error', e);
      });
  };

  if (loginS) {
    return <Redirect to={{ pathname: '/app' }} />;
  }
  if (isloggedin) {
    return <Redirect to={{ pathname: '/app' }} />;
  }
  return (
    <div style={{ fontFamily: 'poppins' }} className='container'>
      <div className='box'>
        <div className='box-1' style={{ elevation: 5 }}>
          <div className='color1122'>
            <img className='imagetag' src={logo} />{' '}
          </div>
          <input
            type='text'
            value={email}
            onChange={(e) => setemail(e.target.value)}
            name=''
            placeholder='Email'
          ></input>
          <input
            type='Password'
            value={password}
            onChange={(e) => setpassword(e.target.value)}
            name=''
            placeholder='Password'
          ></input>
          <input
            type='submit'
            onClick={onSubmit}
            name=''
            value='SIGN IN'
          ></input>
          <div style={{ marginTop: '8%' }}>
            <p className='dont'>Don`t have an Account? </p>
            <Link to='/signup'>
              {' '}
              <p className='sign'>SIGN UP NOW</p>
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
}
