import React from 'react';
import { Modal } from 'antd';
import './style.css';
function Weather(props) {
  return (
    <div>
      <Modal
        title=''
        visible={props.visible}
        onCancel={props.onCancel}
        centered
        className='modal-body2 '
        footer={[]}
      >
        <div>
          {' '}
          <iframe
            src={
              'https://embed.windy.com/embed2.html?lat=29.194&lon=47.576&detailLat=25.493&detailLon=-90.945&width=650&height=450&zoom=7&level=surface&overlay=wind&product=ecmwf&menu=&message=&marker=&calendar=now&pressure=&type=map&location=coordinates&detail=&metricWind=default&metricTemp=default&radarRange=-1'
            }
            className='iframevalues'
            // frameborder='0'
            allowFullScreen='true'
          />{' '}
        </div>
      </Modal>
    </div>
  );
}

export default Weather;
