import React, { useState, useEffect } from 'react';
import { Form, Button, Checkbox, Select, Input, Table } from 'antd';
import { client } from '../../config/index';
import PerfectScrollbar from 'react-perfect-scrollbar';
import './style.css';
import { useSelector } from 'react-redux';
import { useLazyTranslate } from 'react-google-translate';
const { Option } = Select;

function Form1() {
  const [foundData, setfoundData] = useState([]);
  const [flag, setflag] = useState(false);

  const language = useSelector((state) => state.authReducer.setLanguage);
  const [text] = useState([
    'Layer',
    'Pins',
    'Field',
    'Description',
    'Link',
    'Date',
    'Operator',
    'Starts With',
    'Ends With',
    'Contains',
    'Value',
    'Search',
    'Within current map extent',
    'Within polygon',
  ]);

  const [translate, { data, loading }] = useLazyTranslate({
    language,
  });

  useEffect(() => {
    if (text) {
      translate(text, language);
    }
  }, [translate, text, language]);
  const onFinish = (values) => {
    console.log('Success:', values);
    let comingData = [];

    client
      .service('pins')
      .find()
      .then((res) => {
        comingData = res.data;

        if (values.field === 'UAV Link') {
          comingData = comingData.filter(
            (row) => row?.point_party.indexOf(values.value) > -1
          );
        } else if (values.field === 'Description') {
          comingData = comingData.filter(
            (row) => row?.point_description.indexOf(values.value) > -1
          );
        }
        setfoundData(comingData);
        setflag(true);
      })
      .catch((e) => {
        // Show login page (potentially with `e.message`)
        console.error('Authentication error', e);
      });
  };

  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };
  function handleChange(value) {
    console.log(`selected ${value}`);
  }
  const columns = [
    {
      title: 'point_type',
      dataIndex: 'point_type',
    },
    {
      title: 'point_party',
      dataIndex: 'point_party',
    },
    {
      title: 'point_description',
      dataIndex: 'point_description',
    },
  ];
  return (
    <PerfectScrollbar style={{ height: '89vh', backgroundColor: 'white' }}>
      <div>
        <div className='formDiv'>
          <div style={{ paddingTop: '1rem' }}>
            <Form
              name='basic'
              onFinish={onFinish}
              onFinishFailed={onFinishFailed}
              autoComplete='off'
            >
              <p className='fontFamilys'>
                <strong>{loading ? 'Loading...' : data[0]}</strong>
              </p>

              <Form.Item
                name='layer'
                rules={[
                  {
                    required: false,
                    message: 'Please input your layer!',
                  },
                ]}
              >
                <Select style={{ width: 200 }} onChange={handleChange}>
                  <Option value='Pins'>
                    {loading ? 'Loading...' : data[1]}
                  </Option>
                </Select>
              </Form.Item>
              <p className='fontFamilys'>
                <strong>{loading ? 'Loading...' : data[2]}</strong>
              </p>
              <Form.Item
                name='field'
                rules={[
                  {
                    required: true,
                    message: 'Please input your Field!',
                  },
                ]}
              >
                <Select
                  defaultValue='Type'
                  style={{ width: 200 }}
                  onChange={handleChange}
                >
                  <Option value='Description'>
                    {loading ? 'Loading...' : data[3]}
                  </Option>
                  <Option value='UAV Link'>
                    UAV {loading ? 'Loading...' : data[4]}
                  </Option>
                  <Option value='Date'>
                    {loading ? 'Loading...' : data[5]}
                  </Option>
                </Select>
              </Form.Item>
              <p className='fontFamilys'>
                <strong>{loading ? 'Loading...' : data[6]}</strong>
              </p>
              <Form.Item
                name='operator'
                rules={[
                  {
                    required: true,
                    message: 'Please input your Operator!',
                  },
                ]}
              >
                <Select
                  defaultValue='is'
                  style={{ width: 200 }}
                  onChange={handleChange}
                >
                  <Option value='Starts With'>
                    {loading ? 'Loading...' : data[7]}
                  </Option>
                  <Option value='Ends With'>
                    {loading ? 'Loading...' : data[8]}
                  </Option>
                  <Option value='Contains'>
                    {loading ? 'Loading...' : data[9]}
                  </Option>
                </Select>
              </Form.Item>
              <p className='fontFamilys'>
                <strong>{loading ? 'Loading...' : data[10]}</strong>
              </p>
              <Form.Item
                name='value'
                rules={[
                  {
                    required: true,
                    message: 'Please input your value!',
                  },
                ]}
              >
                <Input style={{ width: 200 }} />
              </Form.Item>

              <Form.Item>
                <Button
                  type='primary'
                  htmlType='submit'
                  style={{
                    backgroundColor: '#F7C145',
                    border: 'solid white 1px',
                  }}
                >
                  {loading ? 'Loading...' : data[11]}
                </Button>
              </Form.Item>
            </Form>
            <Checkbox className='fontFamilys'>
              {loading ? 'Loading...' : data[12]}
            </Checkbox>
            <br />
            <Checkbox className='fontFamilys'>
              {' '}
              {loading ? 'Loading...' : data[13]}
            </Checkbox>
          </div>
        </div>
        {flag ? (
          <div style={{ marginTop: '1rem' }}>
            <Table
              pagination={false}
              dataSource={foundData}
              columns={columns}
            />
          </div>
        ) : null}
      </div>
    </PerfectScrollbar>
  );
}

export default Form1;
