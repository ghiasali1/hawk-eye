import React, { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import { useLazyTranslate } from 'react-google-translate';
import './style.css';

function Index(props) {
  const language = useSelector((state) => state.authReducer.setLanguage);
  const [text] = useState(['Use Tools On The Left To Print']);

  const [translate, { data, loading }] = useLazyTranslate({
    language,
  });

  useEffect(() => {
    if (text) {
      translate(text, language);
    }
  }, [translate, text, language]);
  return (
    <div
      style={{
        display: 'flex',
        height: '89vh',
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
      }}
    >
      <div>
        <p className='buttonfscreenshot' onClick={props.getImage}>
          {' '}
          {loading ? 'Loading...' : data[0]}
        </p>
      </div>
    </div>
  );
}

export default Index;
