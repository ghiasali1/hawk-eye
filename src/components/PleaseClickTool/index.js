import React, { useState, useEffect } from 'react';
import './style.css';
import { useSelector } from 'react-redux';
import { useLazyTranslate } from 'react-google-translate';
function Tool() {
  const language = useSelector((state) => state.authReducer.setLanguage);
  const [text] = useState(['Please Click on Any Tool']);

  const [translate, { data, loading }] = useLazyTranslate({
    language,
  });

  useEffect(() => {
    if (text) {
      translate(text, language);
    }
  }, [translate, text, language]);
  return (
    <div className='formDiv1122'>
      <div>
        <p className='fontFamilys'>{loading ? 'Loading...' : data}</p>
      </div>
    </div>
  );
}

export default Tool;
