import React from 'react';
import { Modal, Row, Col } from 'antd';
import './style.css';
import PolygonMap from './DrawAbleMap';
import Form1 from './FlightsForm';
function Flights(props) {
  return (
    <div>
      <Modal
        title=''
        visible={props.visible}
        onCancel={props.onCancel}
        centered
        className='modal-body22'
        footer={[]}
      >
        <Row style={{ width: '100%' }}>
          <Col span={16}>
            {' '}
            <iframe
              src={
                'https://prod.teamgantt.com/gantt/schedule/?ids=2825320&public_keys=eQf9yzf93iwO&zoom=d100&font_size=12&estimated_hours=0&assigned_resources=0&percent_complete=0&documents=0&comments=0&col_width=355&hide_header_tabs=0&menu_view=1&resource_filter=1&name_in_bar=0&name_next_to_bar=0&resource_names=1&resource_hours=1#user=&company=&custom=&date_filter=&hide_completed=false&color_filter='
              }
              className='iframevalues123'
              // frameborder='0'
              allowFullScreen='true'
            />{' '}
          </Col>
          <Col span={8}>
            <div>
              <PolygonMap />
            </div>
            <div>
              <Form1 onCancel={props.onCancel} />
            </div>
          </Col>
        </Row>
      </Modal>
    </div>
  );
}

export default Flights;
