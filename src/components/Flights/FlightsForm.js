import React, { useState, useEffect } from 'react';
import { Form, Button, Select, Input, DatePicker, message } from 'antd';
import './style.css';
import { client } from '../../config/index';
import { useSelector } from 'react-redux';
import { useLazyTranslate } from 'react-google-translate';
const { Option } = Select;

function Form1(props) {
  const [date, setdate] = useState('');
  const language = useSelector((state) => state.authReducer.setLanguage);
  const [text] = useState([
    'Flight Plan Date',
    'Pins',
    'Requested Flight Date',
    'Description',
    'Submit',
  ]);

  const [translate, { data, loading }] = useLazyTranslate({
    language,
  });

  useEffect(() => {
    if (text) {
      translate(text, language);
    }
  }, [translate, text, language]);

  const onFinish = (values) => {
    client
      .service('flights')
      .create({
        flightPlanDate: values.flightPlan,
        description: values.description,
        requestedFlightDate: date,
      })
      .then((res) => {
        message.success('Flights Plan is Added');
        props.onCancel();
      })
      .catch((e) => {
        console.error('Authentication error', e);
      });
  };

  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };
  function handleChange(value) {
    console.log(`selected ${value}`);
  }
  function onChange(date, dateString) {
    console.log(date, dateString);
    setdate(dateString);
  }
  return (
    <div className='formDiv112233'>
      <div style={{ paddingTop: '1rem' }}>
        <Form
          name='basic'
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete='off'
          style={{ lineHeight: '0px' }}
        >
          <p className='fontFamilys11'>
            <strong>{loading ? 'Loading...' : data[0]}</strong>
          </p>

          <Form.Item
            name='flightPlan'
            rules={[
              {
                required: false,
                message: 'Please input your Flight Plan Date!',
              },
            ]}
          >
            <Select
              defaultValue=''
              style={{ width: 270 }}
              onChange={handleChange}
            >
              <Option value='Pins'>{loading ? 'Loading...' : data[1]}</Option>
            </Select>
          </Form.Item>
          <p style={{ marginTop: '-10px' }} className='fontFamilys11'>
            <strong>{loading ? 'Loading...' : data[2]}</strong>
          </p>
          <Form.Item
            name='date'
            rules={[
              {
                required: false,
                message: 'Please input your date!',
              },
            ]}
          >
            <DatePicker style={{ width: 270 }} onChange={onChange} />
          </Form.Item>
          <p style={{ marginTop: '-10px' }} className='fontFamilys11'>
            <strong>{loading ? 'Loading...' : data[3]}</strong>
          </p>
          <Form.Item
            name='description'
            rules={[
              {
                required: false,
                message: 'Please input your Description!',
              },
            ]}
          >
            <Input style={{ width: 270 }} />
          </Form.Item>

          <Form.Item style={{ marginTop: '-15px' }}>
            <Button
              type='primary'
              htmlType='submit'
              style={{
                backgroundColor: 'rgb(246,193,68)',
                border: 'solid white 1px',
                marginTop: '-25px',
              }}
            >
              {loading ? 'Loading...' : data[4]}
            </Button>
          </Form.Item>
        </Form>
      </div>
    </div>
  );
}

export default Form1;
