import React, { useState, useEffect, useRef } from 'react';
import {
  Map,
  TileLayer,
  Marker,
  Popup,
  LayersControl,
  LayerGroup,
  ZoomControl,
  withLeaflet,
} from 'react-leaflet';
import { useDispatch, useSelector } from 'react-redux';
import { setPinSelected, setLatLag } from '../../redux/actions';
import { client } from '../../config/index';
import Img from '../../images/street.jpg';
import 'leaflet/dist/leaflet.css';
import './style.css';
import icon from 'leaflet/dist/images/marker-icon.png';
import iconShadow from 'leaflet/dist/images/marker-shadow.png';
import newicon from '../../images/drop.png';
import drug from '../../images/drug.png';
import ilegal from '../../images/ilegal.png';
import political from '../../images/political.png';
import stage from '../../images/stage.png';
import areaofi from '../../images/areaofi.png';

import L from 'leaflet';
import PrintControlDefault from 'react-leaflet-easyprint';
const PrintControl = withLeaflet(PrintControlDefault);

let DefaultIcon = L.icon({
  iconUrl: icon,
  shadowUrl: iconShadow,
});
L.Marker.prototype.options.icon = DefaultIcon;

// icons starts from here
const iconPerson = new L.Icon({
  iconUrl: newicon,

  iconSize: new L.Point(40, 40),
});

const drug1 = new L.Icon({
  iconUrl: drug,

  iconSize: new L.Point(40, 40),
});
const ilegal1 = new L.Icon({
  iconUrl: ilegal,

  iconSize: new L.Point(40, 40),
});
const political1 = new L.Icon({
  iconUrl: political,

  iconSize: new L.Point(40, 40),
});
const stage1 = new L.Icon({
  iconUrl: stage,

  iconSize: new L.Point(40, 40),
});
const areaofi1 = new L.Icon({
  iconUrl: areaofi,

  iconSize: new L.Point(40, 40),
});

//  components starts from here
const center = [51.505, -0.09];

function Map4(props) {
  const printControlRef = useRef();

  const dispatch = useDispatch();
  const [pinsdata, setpinsdata] = useState([]);
  const [marker, setmarker] = useState(null);
  const pinSelected = useSelector((state) => state.authReducer.pinSelected);

  const onclikedMap = (e) => {
    if (props.panelid === 1) {
      setmarker(e?.latlng);
      dispatch(setPinSelected(true));
      dispatch(setLatLag(e?.latlng));
    }
  };

  useEffect(() => {
    if (!pinSelected) {
      setmarker(null);
    }
    client
      .service('pins')
      .find()
      .then((res) => {
        setpinsdata(res.data);
      })
      .catch((e) => {
        // Show login page (potentially with `e.message`)
        console.error('Authentication error', e);
      });
  }, [pinSelected]);

  return (
    <div>
      <Map
        attributionControl={false}
        center={center}
        zoom={13}
        scrollWheelZoom={true}
        zoomControl={false}
        onclick={(e) => onclikedMap(e)}
      >
        <LayersControl position='topright'>
          <LayersControl.BaseLayer checked name='OpenStreetMap.Mapnik'>
            <TileLayer
              attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
              url='https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png'
            />
          </LayersControl.BaseLayer>
          <LayersControl.BaseLayer name='OpenStreetMap.BlackAndWhite'>
            <TileLayer
              attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
              url='https://tiles.wmflabs.org/bw-mapnik/{z}/{x}/{y}.png'
            />
          </LayersControl.BaseLayer>
          <LayersControl.Overlay name='Feature'>
            <Marker position={center}>
              <Popup>
                <a-scene embedded class='aframebox'>
                  <a-sky src={Img}></a-sky>
                </a-scene>
              </Popup>
            </Marker>
          </LayersControl.Overlay>
          <LayersControl.Overlay name='Pins'>
            <LayerGroup>
              <div>
                {pinsdata?.map((user) => (
                  <div>
                    {user?.point_type === 'Drug House' ? (
                      <Marker
                        icon={drug1}
                        id={user._id}
                        position={user?.latlng}
                      >
                        <Popup>
                          <div
                            style={{ lineHeight: '3px', fontFamily: 'poppins' }}
                          >
                            <div style={{ display: 'flex' }}>
                              <div
                                style={{
                                  border: 'solid grey 1px',
                                  height: '1.5rem',
                                  padding: '1rem',
                                }}
                              >
                                point_type
                              </div>

                              <div
                                style={{
                                  border: 'solid grey 1px',
                                  height: '1.5rem',
                                  padding: '1rem',
                                }}
                              >
                                {user?.point_type}
                              </div>
                            </div>
                            <div style={{ display: 'flex' }}>
                              <div
                                style={{
                                  border: 'solid grey 1px',
                                  height: '1.5rem',
                                  padding: '1rem',
                                }}
                              >
                                point_party
                              </div>

                              <div
                                style={{
                                  border: 'solid grey 1px',
                                  height: '1.5rem',
                                  padding: '1rem',
                                }}
                              >
                                {user?.point_party}
                              </div>
                            </div>
                            <div style={{ display: 'flex' }}>
                              <div
                                style={{
                                  border: 'solid grey 1px',
                                  height: '1.5rem',
                                  padding: '1rem',
                                }}
                              >
                                point_description
                              </div>

                              <div
                                style={{
                                  border: 'solid grey 1px',
                                  height: '1.5rem',
                                  padding: '1rem',
                                }}
                              >
                                {user?.point_description}
                              </div>
                            </div>
                          </div>
                        </Popup>
                      </Marker>
                    ) : null}
                    {user?.point_type === 'Area of intrest' ? (
                      <Marker
                        icon={areaofi1}
                        id={user._id}
                        position={user?.latlng}
                      >
                        <Popup>
                          <div
                            style={{ lineHeight: '3px', fontFamily: 'poppins' }}
                          >
                            <div style={{ display: 'flex' }}>
                              <div
                                style={{
                                  border: 'solid grey 1px',
                                  height: '1.5rem',
                                  padding: '1rem',
                                }}
                              >
                                point_type
                              </div>

                              <div
                                style={{
                                  border: 'solid grey 1px',
                                  height: '1.5rem',
                                  padding: '1rem',
                                }}
                              >
                                {user?.point_type}
                              </div>
                            </div>
                            <div style={{ display: 'flex' }}>
                              <div
                                style={{
                                  border: 'solid grey 1px',
                                  height: '1.5rem',
                                  padding: '1rem',
                                }}
                              >
                                point_party
                              </div>

                              <div
                                style={{
                                  border: 'solid grey 1px',
                                  height: '1.5rem',
                                  padding: '1rem',
                                }}
                              >
                                {user?.point_party}
                              </div>
                            </div>
                            <div style={{ display: 'flex' }}>
                              <div
                                style={{
                                  border: 'solid grey 1px',
                                  height: '1.5rem',
                                  padding: '1rem',
                                }}
                              >
                                point_description
                              </div>

                              <div
                                style={{
                                  border: 'solid grey 1px',
                                  height: '1.5rem',
                                  padding: '1rem',
                                }}
                              >
                                {user?.point_description}
                              </div>
                            </div>
                          </div>
                        </Popup>
                      </Marker>
                    ) : null}
                    {user?.point_type === 'Staging Area' ? (
                      <Marker
                        icon={stage1}
                        id={user._id}
                        position={user?.latlng}
                      >
                        <Popup>
                          <div
                            style={{ lineHeight: '3px', fontFamily: 'poppins' }}
                          >
                            <div style={{ display: 'flex' }}>
                              <div
                                style={{
                                  border: 'solid grey 1px',
                                  height: '1.5rem',
                                  padding: '1rem',
                                }}
                              >
                                point_type
                              </div>

                              <div
                                style={{
                                  border: 'solid grey 1px',
                                  height: '1.5rem',
                                  padding: '1rem',
                                }}
                              >
                                {user?.point_type}
                              </div>
                            </div>
                            <div style={{ display: 'flex' }}>
                              <div
                                style={{
                                  border: 'solid grey 1px',
                                  height: '1.5rem',
                                  padding: '1rem',
                                }}
                              >
                                point_party
                              </div>

                              <div
                                style={{
                                  border: 'solid grey 1px',
                                  height: '1.5rem',
                                  padding: '1rem',
                                }}
                              >
                                {user?.point_party}
                              </div>
                            </div>
                            <div style={{ display: 'flex' }}>
                              <div
                                style={{
                                  border: 'solid grey 1px',
                                  height: '1.5rem',
                                  padding: '1rem',
                                }}
                              >
                                point_description
                              </div>

                              <div
                                style={{
                                  border: 'solid grey 1px',
                                  height: '1.5rem',
                                  padding: '1rem',
                                }}
                              >
                                {user?.point_description}
                              </div>
                            </div>
                          </div>
                        </Popup>
                      </Marker>
                    ) : null}

                    {user?.point_type === 'Political Distubrance' ? (
                      <Marker
                        icon={political1}
                        id={user._id}
                        position={user?.latlng}
                      >
                        <Popup>
                          <div
                            style={{ lineHeight: '3px', fontFamily: 'poppins' }}
                          >
                            <div style={{ display: 'flex' }}>
                              <div
                                style={{
                                  border: 'solid grey 1px',
                                  height: '1.5rem',
                                  padding: '1rem',
                                }}
                              >
                                point_type
                              </div>

                              <div
                                style={{
                                  border: 'solid grey 1px',
                                  height: '1.5rem',
                                  padding: '1rem',
                                }}
                              >
                                {user?.point_type}
                              </div>
                            </div>
                            <div style={{ display: 'flex' }}>
                              <div
                                style={{
                                  border: 'solid grey 1px',
                                  height: '1.5rem',
                                  padding: '1rem',
                                }}
                              >
                                point_party
                              </div>

                              <div
                                style={{
                                  border: 'solid grey 1px',
                                  height: '1.5rem',
                                  padding: '1rem',
                                }}
                              >
                                {user?.point_party}
                              </div>
                            </div>
                            <div style={{ display: 'flex' }}>
                              <div
                                style={{
                                  border: 'solid grey 1px',
                                  height: '1.5rem',
                                  padding: '1rem',
                                }}
                              >
                                point_description
                              </div>

                              <div
                                style={{
                                  border: 'solid grey 1px',
                                  height: '1.5rem',
                                  padding: '1rem',
                                }}
                              >
                                {user?.point_description}
                              </div>
                            </div>
                          </div>
                        </Popup>
                      </Marker>
                    ) : null}
                    {user?.point_type === 'Illegal Crossing' ? (
                      <Marker
                        icon={ilegal1}
                        id={user._id}
                        position={user?.latlng}
                      >
                        <Popup>
                          <div
                            style={{ lineHeight: '3px', fontFamily: 'poppins' }}
                          >
                            <div style={{ display: 'flex' }}>
                              <div
                                style={{
                                  border: 'solid grey 1px',
                                  height: '1.5rem',
                                  padding: '1rem',
                                }}
                              >
                                point_type
                              </div>

                              <div
                                style={{
                                  border: 'solid grey 1px',
                                  height: '1.5rem',
                                  padding: '1rem',
                                }}
                              >
                                {user?.point_type}
                              </div>
                            </div>
                            <div style={{ display: 'flex' }}>
                              <div
                                style={{
                                  border: 'solid grey 1px',
                                  height: '1.5rem',
                                  padding: '1rem',
                                }}
                              >
                                point_party
                              </div>

                              <div
                                style={{
                                  border: 'solid grey 1px',
                                  height: '1.5rem',
                                  padding: '1rem',
                                }}
                              >
                                {user?.point_party}
                              </div>
                            </div>
                            <div style={{ display: 'flex' }}>
                              <div
                                style={{
                                  border: 'solid grey 1px',
                                  height: '1.5rem',
                                  padding: '1rem',
                                }}
                              >
                                point_description
                              </div>

                              <div
                                style={{
                                  border: 'solid grey 1px',
                                  height: '1.5rem',
                                  padding: '1rem',
                                }}
                              >
                                {user?.point_description}
                              </div>
                            </div>
                          </div>
                        </Popup>
                      </Marker>
                    ) : null}
                  </div>
                ))}
              </div>
            </LayerGroup>
          </LayersControl.Overlay>
        </LayersControl>
        {props.panelid === 1 ? (
          <div>
            {marker !== null ? (
              <Marker icon={iconPerson} position={marker}>
                <Popup>
                  <div style={{ lineHeight: '3px', fontFamily: 'poppins' }}>
                    <p>Latitude: {marker?.lat}</p>
                    <p>Longitude: {marker?.lng} </p>
                  </div>
                </Popup>
              </Marker>
            ) : null}
          </div>
        ) : null}
        <ZoomControl position='bottomright' />
        {props.panelid === 8 ? (
          <div>
            {' '}
            <PrintControl
              ref={printControlRef}
              position='topleft'
              sizeModes={['Current', 'A4Portrait', 'A4Landscape']}
              hideControlContainer={false}
            />
            <PrintControl
              position='topleft'
              sizeModes={['Current', 'A4Portrait', 'A4Landscape']}
              hideControlContainer={false}
              title='Export as PNG'
              exportOnly
            />
          </div>
        ) : null}
      </Map>
    </div>
  );
}

export default Map4;
