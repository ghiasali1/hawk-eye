import React, { useState, useEffect } from 'react';

import { Layout, Menu } from 'antd';
import MenuIcon from '@mui/icons-material/Menu';

import { IconButton } from '@mui/material';
import { SearchOutlined } from '@ant-design/icons';
import './style.css';

import HeaderMap from '../../Container/HeaderMap/HeaderMap';
import Form1 from '../../components/Form/Form';
import SiderVideo from '../Videos/siderVideo';
import Analytics from '../Analytics/Analytics';
import Weather from '../Weather/Weather';
import Flights from '../Flights/Flights';
import DropPins from '../Droppins/DropPins';
import Screenshot from '../Screehnshot/index';
import Addperson from '../PersonPin/index';
import { useSelector, useDispatch } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { setPinSelected } from '../../redux/actions';
import Location from '../../images/logos/map-marker.svg';
import Searchcc from '../../images/logos/search.svg';
import Plane from '../../images/logos/plane.svg';
import Video from '../../images/logos/video.svg';
import StreetView from '../../images/logos/street-view.svg';
import Thunder from '../../images/logos/thunderstorm-sun.svg';
import Analytics1 from '../../images/logos/analytics.svg';
import Save from '../../images/logos/save.svg';
import HeaderMap2 from '../../Container/HeaderMap/HeaderMap2';
import Tool from '../PleaseClickTool';
import { useLazyTranslate } from 'react-google-translate';

const { Content, Sider } = Layout;

function Sider2() {
  const dispatch = useDispatch();

  const isloggedin = useSelector((state) => state.authReducer.isLoggedIn);
  const user = useSelector((state) => state.authReducer.userProfile);

  const [collapsed, setcollapsed] = useState(false);
  const [collapsed1, setcollapsed1] = useState(true);

  const [panelid, setpanelid] = useState(0);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isModalVisible1, setIsModalVisible1] = useState(false);
  const [isModalVisible2, setIsModalVisible2] = useState(false);
  const language = useSelector((state) => state.authReducer.setLanguage);
  const [text] = useState(['Search Here...']);

  const [translate, { data, loading }] = useLazyTranslate({
    language,
  });

  useEffect(() => {
    if (text) {
      translate(text, language);
    }
  }, [translate, text, language]);
  useEffect(() => {
    if (panelid !== 1) {
      dispatch(setPinSelected(false));
    }
  }, [panelid]);
  if (!isloggedin) {
    return <Redirect to={{ pathname: '/' }} />;
  }

  // PowerBimodal

  function showModal() {
    setIsModalVisible(true);
  }
  const handleCancel = () => {
    setIsModalVisible(false);
  };
  // weathermodal
  function showModal1() {
    setIsModalVisible1(true);
  }
  const handleCancel1 = () => {
    setIsModalVisible1(false);
  };

  // for flights modal
  function showModal2() {
    setIsModalVisible2(true);
  }
  const handleCancel2 = () => {
    setIsModalVisible2(false);
  };

  const onCollapse = () => {
    setcollapsed(!collapsed);
  };

  const setAllFor = (index) => {
    setcollapsed(true);
    setpanelid(index);
  };
  return (
    <div>
      <Layout style={{ minHeight: '100vh' }}>
        <Sider
          style={{
            backgroundColor: '#132F58',
            overflow: 'auto',
            height: '100vh',
            position: 'fixed',
            left: 0,
          }}
          collapsible
          collapsed={collapsed1}
        >
          <div
            style={{
              display: 'flex',
              justifyContent: 'center',
              marginTop: '1rem',
            }}
          >
            <IconButton onClick={onCollapse}>
              <MenuIcon style={{ color: '#fff' }} />
            </IconButton>
          </div>
          <Menu
            style={{ backgroundColor: '#132F58' }}
            theme='dark'
            defaultSelectedKeys={['0']}
            mode='inline'
          >
            <Menu.Item
              key='1'
              onClick={() => setAllFor(1)}
              // icon={<RoomIcon style={{ fontSize: '30px' }} />}
            >
              <img className='iconstageee' src={Location} alt='icon' />
            </Menu.Item>
            <Menu.Item key='2' onClick={() => setAllFor(2)}>
              <img className='iconstageee1' src={Searchcc} alt='icon' />
            </Menu.Item>
            <Menu.Item key='3' onClick={showModal2}>
              <img className='iconstageee1' src={Plane} alt='icon' />
            </Menu.Item>
            <Menu.Item key='4' onClick={() => setAllFor(4)}>
              <img className='iconstageee1' src={Video} alt='icon' />
            </Menu.Item>
            <Menu.Item key='5' onClick={() => setAllFor(5)}>
              <img className='iconstageee1' src={StreetView} alt='icon' />
            </Menu.Item>
            <Menu.Item key='6' onClick={showModal1}>
              <img className='iconstageee1' src={Thunder} alt='icon' />
            </Menu.Item>
            <Menu.Item key='7' onClick={showModal}>
              <img className='iconstageee1' src={Analytics1} alt='icon' />
            </Menu.Item>
            <Menu.Item key='8' onClick={() => setAllFor(8)}>
              <img className='iconstageee1' src={Save} alt='icon' />
            </Menu.Item>
          </Menu>
        </Sider>
        <Layout className='site-layout' style={{ marginLeft: 80 }}>
          <Content>
            <div style={{ display: 'flex' }}>
              {collapsed ? (
                <div style={{ width: '20vw' }}>
                  <div class='input-icons'>
                    <i>
                      <SearchOutlined className='icon' />
                    </i>
                    <input
                      class='input-field'
                      type='text'
                      placeholder={loading ? 'Loading...' : data[0]}
                    />
                  </div>
                  {panelid === 2 ? <Form1 /> : null}
                  {panelid === 4 ? <SiderVideo /> : null}
                  {panelid === 1 ? <DropPins panelid={panelid} /> : null}
                  {panelid === 8 ? <Screenshot /> : null}
                  {panelid === 5 ? <Addperson /> : null}
                  {panelid === 0 ? <Tool /> : null}
                </div>
              ) : null}
              {collapsed ? (
                <div>
                  <HeaderMap user={user} panelid={panelid} />
                </div>
              ) : (
                <div>
                  <HeaderMap2 user={user} panelid={panelid} />
                </div>
              )}
            </div>
          </Content>
        </Layout>
      </Layout>
      <Analytics visible={isModalVisible} onCancel={handleCancel} />
      <Weather visible={isModalVisible1} onCancel={handleCancel1} />
      <Flights visible={isModalVisible2} onCancel={handleCancel2} />
    </div>
  );
}

export default Sider2;
