import React from 'react';
import { Modal } from 'antd';
import './style.css';
function Analytics(props) {
  return (
    <div>
      <Modal
        title=''
        visible={props.visible}
        onCancel={props.onCancel}
        centered
        className='modal-body2'
        footer={[]}
      >
        <div>
          {' '}
          <iframe
            src={
              'https://app.powerbi.com/view?r=eyJrIjoiYTJlZWZkZmYtOTg3Yi00MWMzLWE1MTAtZGYwMzIwYjY3YjdlIiwidCI6IjU3NGMzZTU2LTQ5MjQtNDAwNC1hZDFhLWQ4NDI3ZTdkYjI0MSIsImMiOjZ9'
            }
            className='iframevalues12'
            // frameborder='0'
            allowFullScreen='true'
          />{' '}
        </div>
      </Modal>
    </div>
  );
}

export default Analytics;
