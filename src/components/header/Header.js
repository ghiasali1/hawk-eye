import React, { useState, useEffect } from 'react';
import './style.css';
import logo from '../../images/logo.png';
import {
  InfoCircleOutlined,
  BellOutlined,
  DownOutlined,
  LoginOutlined,
} from '@ant-design/icons';
import { Menu, Dropdown, Avatar } from 'antd';
import { client } from '../../config/index';
import { setLanguage } from '../../redux/actions';

import { useSelector, useDispatch } from 'react-redux';
import { useLazyTranslate } from 'react-google-translate';

function Header(props) {
  const dispatch = useDispatch();

  const language = useSelector((state) => state.authReducer.setLanguage);
  const [text] = useState(['Welcome', 'Logout']);

  const [translate, { data, loading }] = useLazyTranslate({
    language,
  });

  useEffect(() => {
    if (text) {
      translate(text, language);
    }
  }, [translate, text, language]);

  const setLang = (g) => {
    dispatch(setLanguage(g));
  };
  const menu = (
    <Menu>
      <Menu.Item onClick={() => setLang('en')}>
        <Avatar
          style={{ marginRight: '4px' }}
          size={24}
          src='https://cdn.britannica.com/79/4479-050-6EF87027/flag-Stars-and-Stripes-May-1-1795.jpg'
        />
        <a>English(US)</a>
      </Menu.Item>
      <Menu.Item onClick={() => setLang('ur')}>
        <Avatar
          style={{ marginRight: '4px' }}
          size={24}
          src='https://upload.wikimedia.org/wikipedia/commons/thumb/3/32/Flag_of_Pakistan.svg/1280px-Flag_of_Pakistan.svg.png'
        />
        <a>Urdu</a>
      </Menu.Item>
      <Menu.Item onClick={() => setLang('ar')}>
        <Avatar
          style={{ marginRight: '4px' }}
          size={24}
          src='https://cdn.britannica.com/79/5779-004-DC479508/Flag-Saudi-Arabia.jpg'
        />
        <a>Arabic</a>
      </Menu.Item>
      <Menu.Item onClick={() => setLang('de')}>
        <Avatar
          style={{ marginRight: '4px' }}
          size={24}
          src='https://upload.wikimedia.org/wikipedia/en/thumb/b/ba/Flag_of_Germany.svg/1200px-Flag_of_Germany.svg.png'
        />
        <a>German</a>
      </Menu.Item>
      <Menu.Item onClick={() => setLang('hi')}>
        <Avatar
          style={{ marginRight: '4px' }}
          size={24}
          src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAARMAAAC3CAMAAAAGjUrGAAAAmVBMVEX/mTMSiAf/////lycAgQAAAIgAAIUAAH4AAIIAAIMAAHwAAHPi4u+kpMvz8/kAAHjn5/Lt7fXd3ey5uddPT6GZmcWDg7n6+v7Ly+HIyOJERJxlZao+PponJ5L29vy0tNbW1uggIJCfn8iGhrsYGJBNTaB2drQQEIx+fripqc5ra62QkMBXV6QiIpFfX6i+vtwyMpw6Opg1NZe0gadDAAAEvUlEQVR4nO3ba3OiSBiG4UzvdDcip8YDiAdQZAIxExP//4/btzHjZH1Nze6HpVPlc1Ul0eiH9g7Q2JKHBwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACA/+gvuPYg4BqacGjCoQmHJhyacF+hSTI5LFd7uV8tD5PE9WDEF2gSHNbaGyklpVRq5Omfh8D1kBw3qaZ6pCiGVFrr/oYc6SZ3OyinTZIpdZCeevnhNyY1jf/jRXm0vejG6S7kssnObhmeNxMbcTSiEuZIt2aeZ7eancNxOWzy7NFrn3cnI/p9pbTfcmFO3ZxaeVN3A3PWJNrS5lAEmb3d5wgvt7KgoMe2kauhuWoSPdK2QHvIwt5ZpPTtQF/p+S7tVbQNPbqK4qhJRluJ17VGZB3di2yPF/o62AxdJkzbUZRt5mZwjpo0lOTY3wrtFGMPHq/vP5N+JxJHiuLomOKmycyTXinOUewLX1IYSTmW7/cpiSjpOTMno3PSJNA049TnQ4gIae8J6cVrSkWbSNdvJvRITbOPdnJO66RJo1QzM3RjaY8YMhLVVmR+JraViGhzEZn9vZnZp7kYnosmCy31+UR180oH1a4QWWyiODJxJhqajaPTpn80oee5OM130WSlVL3pZ11hJO0daiLWITUJ12KiaM+Spn9ssamVKhyMz0GTVNsDrOi6/o5fiVKLdmVis2qFLkXl27OV/rhCh1mdDj9AB01qmoftnz+Z2ir5+Cjk08JP49RfPElxHNvdpZvao2vhyVE9/AAdNHmkOfZ8ihpK2l4m4y4fJ7ryKp2M8248oe1D7vrHDc3Z++EHOHwTmojlr9tRISciHB/beZ3v87puj+OdmMiV+fUE6WI6Hr5J6amnaFEuzptK6av8GAfrKmzDah3Ex/zR798Iik1e5tGT8srBRzh8k7lSLf1ID9NlWNF5yHLcBuugLd6KNl0H87E9N0m7p+Zgj66tUvPBRzh8k2cl/fNZRzLbxut2kczXaWFXIEdFsG6TfH6KH2fnPSb3pXoefITDN3lVanpZBTDhytd12o6kNWrTg/ZPu8vKYzRV6nXwEQ7fRErvkiSLIpNWeViovokqdnmVmii6LBJE3u8D8mCcNEmqYzhbnqT2/TiOvX37Js/e6q1Hv4l9LX8+H8IyT++mCbaTK1fHkzccTz7OO8Hveafp550mWM/vct6Zn085cH7ywe3z2JTOY9O7PY/F+50b9vS++PyiP3tfPLm398WiHv15/aTs109Wd7N+gnW2G4p/uR6b3c96rMiv1u2bT9btgztat+8/3zmY989xbn6+I+7t8x18DngLPi++YYrrChhcf3IDrlO6IdqOpCpSXM/2D/11j234yXWPwy+bXOD6WO5rXkc9vdvrqAWut78N/5dx08f/3zF/fvr/7is0+WrQhEMTDk04NOHQhHv4DtcevsE1NOHQhEMTDk04NOHQhEMTDk04NOHQhEMTDk04NOHQhEMTDk04NOHQhEMTDk04NOHQhEMTDk04NOHQhEMTDk04NOHQhEMTDk04NOHQhEMTDk04NOHQhEMTDk04NOHQhEMTDk04NOHQhEMTDk04NOHQhEMTDk04NOHQhEMTDk04NOHQhEMT7m/cNifkUwM0UQAAAABJRU5ErkJggg=='
        />
        <a>Hindi</a>
      </Menu.Item>
      <Menu.Item onClick={() => setLang('it')}>
        <Avatar
          style={{ marginRight: '4px' }}
          size={24}
          src='https://cdn.britannica.com/59/1759-004-F4175463/Flag-Italy.jpg'
        />
        <a>Italian</a>
      </Menu.Item>
      <Menu.Item onClick={() => setLang('pt')}>
        <Avatar
          style={{ marginRight: '4px' }}
          size={24}
          src='https://cdn.britannica.com/88/3588-004-E0E45339/Flag-Portugal.jpg'
        />
        <a>Portuguese</a>
      </Menu.Item>
      <Menu.Item onClick={() => setLang('ru')}>
        <Avatar
          style={{ marginRight: '4px' }}
          size={24}
          src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAARIAAAC4CAMAAAAYGZMtAAAAM1BMVEX///8AOKXVKx7q6urnKgDWLyMAKqEAOKdQNpKTn84HPKba2trx8OyHlcUAMaQAPanoLQmtwTivAAABg0lEQVR4nO3Qy1XDUBTAwBtMCDjf/quFJc8qABYzFehorhuL62zDYrPkyJKwJCwJS8KSsCQsCUvCkrAkLAlLwpKwJCwJS8KSsCQsCUvCkrAkLAlLwpKwJCwJS8KSsCQsCUvCkrAkLAlLwpKwJCwJS8KSsCQsCUvCkrAkLAlLwpKwJCwJS8KSsCQsCUvCktjm9tcJ/81t7l8s7vP4YPGY/cRit+TIkrAkLAlLwpKwJCwJS8KSsCQsCUvCkrAkLAlLwpKwJCwJS8KSsCQsCUvCkrAkLAlLwpKwJCwJS8KSsCQsCUvCkrAkLAlLwpKwJCwJS8KSsCQsCUvCkrAkLAlLwpKwJCwJS2Kf5+nCL6fnfHIwr3cWrzm/sThbcmRJWBKWhCVhSVgSloQlYUlYEpaEJWFJWBKWhCVhSVgSloQlYUlYEpaEJWFJWBKWhCVhSVgSloQlYUlYEpaEJWFJWBKWhCVhSVgSloQlYUlYEpaEJWFJWBKWhCVhSVgSloQlYUn8LOHgG0Xm8InP9LwRAAAAAElFTkSuQmCC'
        />
        <a>Russian</a>
      </Menu.Item>
      <Menu.Item onClick={() => setLang('sr')}>
        <Avatar
          style={{ marginRight: '4px' }}
          size={24}
          src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAARMAAAC3CAMAAAAGjUrGAAAB71BMVEX////GNjwMQHbHNjoAQXkAPHREYosANHEANnHEKTCQp73Ozs3ZtrvQSkzBOEHErq7kra6lCxTGGSO2iIjEmirkqK/Ly8urERr5///HLDPW1tbz8/OsHSPtuS7s7Oz39/f1vy/d3d3JLz3m5ubUpizDniu+v77GIyvHzs2dFhvb29ujQkTGLT3isS3mqq+vr66mTU+lMjW7Izu7kymjAACam5nw09T5xi7Uqyquci3Glizi7u3HsoenIyiMawC9miiuNzufVleyfiylbiyiXS6lUDGUTi6dfyavkCeiJDahPzPRqkyQcybAsrKxDRemp6aEZGTFDxqyd3jRam6/Gz2fYi2yWjGCL0FeUkJKLVRnM0dpY1MALGVOTkgAJmmhMSyaY2SPdnQHNmJXZ38AF2AsHD4AHkOEj5++n6CbhoWhjFucgzywqIu4jQmtm3WdfAXJw7ivcxmpSSihcxvBpmnMnADJuJWNeUV9WgCclYG5lj/mrgCdk33JuJO7tKSfRh2PQUKEYieSPi9zRCiMUhSdRDFuXjRJTVGwYWeNKjGJXCmvYipsVx6nZgt3aB+RPxSUVENpODWkCTgyRFumhzlYWVnX4fMAAAB6e3mJAACFUlKfkZCBGhxpcnBlISBDFBDgh4eIfGDbjpHSbnLMU1hbuXmoAAAU1UlEQVR4nO2djX/TRprHR6d2wasSE1urWLJly5IsEcfClkTiN9w4ToxlE9vETbh22+Wlt3drKCaUEnqFPbakpU0IZQ+6vQuld9fj+ENvZmQ7DmGX+3xQLq7w7wNEHklI880zz/PMi2RAjPSiwEHfwBBqxGSvRkz2asRkr0ZM9mrEZK9GTPZqxGSvRkz2asRkr0ZM9mrEZK9GTPZqxGSv/h+YiPt/CWe1/0xq84l9v4az2ncmRia98AuzlP1mUjTKseXqL8tS9plJMZPLaVl+5RdlKfvM5HyGzmml9PyIyYCKxtmYqI/azqDYv3/f+oAg9/kqzmqfmZC/PfyhfOjj/b2I09pvO/noo99Z5w799hdlKPvDhCRJlVRVVj2fTgu8kC6hbdbj+WU0ov1gwrLFqRSTYqgL+Yvnzn38D78/9I+c0spf8LYnUh52+LE4z8Tj8c/lwTGgAJAEyj/9HoA/nPsdgB+TAJZebE+pw07FWSYk6ZlsXwIRJnA5+cnly9aVh3KHoa6urV6F26vTV65BNFR7Y2K4jcVBJiTLpkLcMRBmmp13K59mt86Wr5+KrQXBSdm8fr18duvTz95d/TyuACk/lyqQw4vFMSYkWWxdAMeSCpfsrJYNEYkQ9dgaxZ2kSwkCFyT0bOeTqMJMA25qbmiNxSkm5FSBAslIBYCTnYHBAT12Ow5uyKV+CaTUCYKr16BzmUp5HLq4w3KGCakWQGA6cvndTlORLWNnR3V2LQBuytmd/o64JV/lFjurjNQE3iXWkcs7LEeYkMV8Mhhe7eRKnwNgVgZ6N9VZoUndogeZnF27Bq515tc6X4Bk3j+MUBxhos6BwD93SnqibEnM2mx1Zw+0E/CCnZTWrka+MBPGQubdO4BaGkKn4gQTTz36x88zNWgehnUreK0zMFqix4QmZ8Z2/AlRtW4Ek2tb0P8apdWTzfzU8EFxgslEANyI6QiEWPoCSGuzfYcilmPITrRKn5KY7ZwEUkdH20auI4EhbD1OMGHnFNpuMGK5c4W70in1EIhZ+jYTvCHsuF199jSn3LJdjljq/LGpOnADDsuRtuOFTtN2GdXZm0C6PXuvuydhybfj0g2hX2CY8kkQWe0eHPsLACEHbsBhOcGE3OD+uBbDzUEsmXeYk3Kv9STmZzsc8y+0WbQ/iwsxmWFu2McmTPqWUh/CHMWZuOOHhkIjDtCB3IGJbD/OiKcqa7cri/2mU5y9ei28ipsO5HMLSEtOXN9hOcKEvRD44nashjbFe7NrfOUUZEL+icSMaJlGDMjrBbi7uGimKzmMCJrJapgZQjNxKI9lvwTMWhVPg+oVk6Y/M4pE4dgFduyuXtK0jL6yToSUFkvotZhp0l+J6EDx08sgOYxm4lRuv3Hz5Oo6KX49SVTnNUGrVVbEDUCpVUurLlbWs7H57sd7OU2L1Yrs1wVirHL55J3hC8SEY33A87FVeqGoMvnQw4WVyt2tjFWeAlQoQ2vWN98uapq8sAHqoZIpr1cqm5V7bZASNy268tlQzvs41S827vH3F0McuGjyi++/X+HpTEhqFgaYrIMLWzFBfvDtA5n+TgJflu/TOWM4530cGz9J3H1YyVwET2O89f77dzOxcs2ki7DtbFYqp0qx+UQ29lnVylQefP+Qj30H4h3r4cOaU9d2WM6Ns+mHH8T+fOfyZiW3aGVrli7mSuLY3fOP+XR2eWVdrKar4sPSZsy6L5/6+tq/VsxvPxhKZ0I4yYR89KBS3pKXT+k5QagZRJXXdFEUy2mTXhbF5RKfFceWaSGt3zfKmWpl8ZuiY5d2WA6OxyZi6+WzfGl5OUPLmr5cyljavcd6Rq6k56vVkrDI60YpXRHKYk2LVWvyvVf/jwckB5mIC5nHOSuWywqmqfEZwZLlNC/wpkYLvIA+8Zm0afJyVlvkqysZ49X/4wHJybmMxDwvmJqpybJA07TJCzJM32hB0OwfMi6z+Bgt0HxmaFuOw/M7RgVWWpBlWPu/Jlmj0SGx9aHMTGw5yiQxj6stazAheQkXvEPDO2Z1J6/rsBxlMoYaCW1VYO0tUxOE01egSZgWtIzTNwVBiFkmBLKIjjHPvil2QhQtWhZuXhPoxXLtPi2fvimb3xw69L1Jn77Jm5VybcHS6H+7zdPmUC/mcpQJ+cGhj03a7JiaATOSjGyuVdIfH377nKlVbi+moWmI2YzZoeUPD71dcPK6DstRJuzHb537i0zfoumFanXFlE8rAl358IdvaO1GUjAruqGbtAWDz0eH39ocvuH6vpy1k83vPzd52Dqg65iN8Tx9FbpUy6I17fRlntdmrVktffNOGnL6YIiROL3WolaBDrSzE2dwpEFRqB+HOnCveVccaibka2psl07B5ENIWxWo2B6hYguGI4G3jF1nEa97E84KvPVaOvzro7t0/DdYx6F+s1f90uO7z4ofer27cFjg715Lh39NeF4in8+nvqRYfXmx751Dr3cXDuu1mRzpeQaS9XjwtmgU215/qvHCujVW1Vttb6s7tsYOLPbzuJUJW2i122MkkdBPaZ8AqEB9Su3Xm1Qn/HFUem2tUoM9Yrblbad60NzKpNEOwBrPkeJ8WqPT000OAaBahQk0mNYYT3kj8HNQadK8KcA+sRpFu3XWzUwaDGIgNQgjw8MYLDf/XZmWsLVcYonJPOIFlOQPP1wRTIFOl0XWj3emWPcy8SmoiqDFEomSwPO8cCOpBGFBGIA2ZAJABH1SlOZtHqkKIeYxFLz6xJVM7N86iDdgBasZVGvhCi6RqIshtP4viZsSCK8JmsALaM0O2wJdy3InE1IP9MwERp0aLz8+lb5N4SLA+ND6FHs7YKbNxwtpe9GSbSigrbqTiVq361xADaFYLJdF8bHeCduFKZLw2VYCTn732CCNeWMTMemeFNBJNzIhdTDARLxbNgx9K0dftgvrKjllb3GruWy5alQ/Oz/ABHhVNzLxtLtMNnAUSWStDA97fGt26wmMe7z27ls0HdO0WRMjIXzxboOacCMT2zMEkUng2ooJY5HPmLk7XVK+KP4ZWc3F+Mxmwh5hQ8YT7noh9zFhU7hqMBoHCr20daxqJNiu+bTH7Z/BCTFRrfaOQE0njP2MorqPiYrrzti+Y6B/03MjzFzPsezZydiNR3UfE65fPTA3OC0+bkdoyU5edi+EbdjtCZ+Vch8TXHUbCYgUBvrCDckunAZdv7GzS+26XXya331jBfqOmcB42xhgEgE9T/OCnbAXuodHkI3Vl1zHBCXp0W6CBivo61e8BXZpBxebCvQKEUvKdUwIv101iaGwQVzqulKyENnNZGcP7gqEkxQ8C9lJ/oTbmJB1mwn8k4wO+NkGB1MWWAAzt7AEN8I7e3C2pnDYulBi5z4mEAYVRF4jHOaCPT9LNhSEKRxGT9BSEqw6g0YG2L5/VRCNpO1sdNcxgUYwjW0kHrHdKXQcZBE2imgTMExYiQDYSKYjUQggAGOP7V8hRchkGp4BT9lwG5Ningkz4WkugirJYChKQz0T56hgkwEcB5igFEkCjmI4iktOqjiDC0/jVsMlqXCU4fxuYzJ2Cf6+JS4awU4lgjt+zeljSAr8yyjNbUbpfTqmbGPPy1DYAVGQGAOCT9zGhGyj6lHBKMrNoGEgl3Im1FPjaViSpPCTfsGTeM+ZNKFlYTQB9/lYL2YC/05jp7mNmLB4AoclewnrOJr+QTMb5CQMRZwCpDjgcMtC5uI+JnUQ5CATSolGwpBJUsFMPC1vu1Vge0zIiVTb24YRCTKJSkkQbAIKel0pjJiMu5BJOA6ZwCSEYzggKbBlnGFhEktFUVemy2QiDwLQ2arEpBRFrQw6IGRa8CS3MonaTJqoCUEsyTOkSv04teQfYBKIjod+knSykGwCfGAUGRf0Qi5nAitKSbAVSWdYjzcKJk4EBpi0fPVwIEROStMgDhQUiN3PJIL+wRGFiUN/EvK21V12IoVSDEzuJ+NM3O4JuJmJFwSRP4mgOuLkFFDIx6am1PogE6B7NiYIotAEcfs4zjYuV8adbixGoadbV/CIhD42MDEx2HbacDPqIQpJfESXiRR0JROi/QITBsVi6GO9Pn3QTrxLG62ojvOTHTvB+UnEdUyO+F9ggvMTj1dq+QOgPdB2lFYb+di4ndWDSNw+gwGS68aUjqT6TBQMZBq1HTLEBPKMv9jP2Ta80UD+AvSxCoCZHfRAUjSIIhX0z4r7mEwinxBH3eJpBASm7NKPKsv6Gks+le3ayQlW9S01lmDx5DQaOoAdHYqKRuxhlUvuG6NOHbMjK6dE4kqEo8ISte3vyx683vl8BobiCEVxQQb2ejCT4FMXMglyMA4zQIl0x82avdmLl4mLx6NoRAn1jAA+j2Hc13amoIUk4W+82Qso1N9iQnEwuYsGAXK1YRiMk0k3zmXoMMuINJWkAvrB+G8ygc0Mdoe5CPQ8TQrlvS5kMom7xDtG8komFBq6xidEcDhuu8+fjAcAtwPkVW0njtPXcPdQzMSFc6ONfD9d6/05M9WTbq9G2uwXbKKuYjQ4yKTlPiYqZTPBKYrdds6wJNvQCw11Z5xN9Y3rkyqJcntm51A8wODCdQWw+4uZBKODTKbyAOSVVI+Jp43Gpp96CJjbM4MmBalMuo+Jp20zsbt0eHYP9gHr9clUfNdYgX9yLlBAdqIMMqFA3oVrctDQK2oNvbE2cBFcZFXmUrQxPjhW4F/y18OwXxyM13cz4Vy4doucwvlGv47gCfcDS6YC+cbUoJ3UfX5Q95A/KX6vfaQUto+vu5EJDMbUwO89cKJ+LESwJxoqs6vtTKlPfAT5Ixhv9+nZ6YkL1z2SqrSLiXICBJ+QRYlR5waZUL5WwM+y06ANOwPdkUoYkzlXrgUlVcZmgpuPEi9uAHDGpyQDu8ft/UteKliALpZbUp4CPHaCMpUI0EkXMukFHgllYvUGWvm63WD8T8Z3jVFH5k78FCz8GAWg4NH7mSxsauNuXEfN+u0wjKNOm51A7uVRCgQgpQu9/CSkBGCBcoJj0GK/8QEmkivX27MbqKsbtEdYW2gJsQKiT/TWxvmdPJZQx1OtuaVtEGRAuKFKYanHhHIlEzIFmcBK4q5goQjCTRhl/yOElhUMrCtAr79/BJPZJgW8S3XMz3bJLmTynIQJCheBTGA9OWrpIhfEi5C2QySxiwnB/mcQLQQMUsfGN3qRCqUnhMvy2Lf/8J79/E44jDo88dRSFK2jQKMF24VddkKy/4VXtiGPMrdE9YYXgNdDqPUhY/L2a+nwr35mMROY3EeD0pc+NNvDcWgBUmr7kYfs94vZJ9uT0A8rDAOPlBqhp7uZvN5dOCzwq9fUcQ9mgvvDbV8KPYHCoVSM84UCyUcnbCZPftrO+9WWPQmURMvJCz0mqO0or3sTzuqvj4j9H3XUQ6ZsJl4llIJx+IFdW5iz+kE8OT09vQ3/HkNPiMIojdK7O1fCoO5rn7FXUddVcuy1b2LINKPCWIznO1sqWax21v4bVTUJpkjIoImWEiswG4lG/SyhUhzuBrx7+88FwoMeD0PzxSr7PwddB8e1hJ4upuLowT5CXNCsk+AS7AcqIRhoLoWjkTgTjTCAOaaTBNue8oI6F1+roJf4occIn8I+j+o5ftBVcFzPYZYW0L3ogXOimqmUqsVG4AKLXtHu48JJsB0G05SkoGdqiQI5IfmXUpuz8opIkIUARAdze3XmoKvguJ4tcVKKTTENgkiUKqX1+a2a/W4G6GeoqIJ6QRSDczNUFqpuLWTXLfQ2d/ZCUm14Qcp30DVwXjPqlFrVqx4xYSykF1cEM1OyX5brY9CMKWhGkkCBuYv9xRji3Rxvyl9pdDWREI3zVU+h+N5B12Af5BEX0paWy+VogU9ncpnN7tOyczDhD0oRhUH5GeTTfdJ2eTFjCoKglXI5eZafNdSjB12BfdAzVq9auVJJFvhM2Uj03oGqIjPh7JESnMn3vkElYSSqJYEXSqVSbL1a9L347JMbNOMjyOq9rZxgCrpIiPe7L96esOfC8KgAXgLYfSBQLENfsmxmMlq2XEsQ7M8Hff/7oudkomKm5U3ZTMAa977W2qNgO8EjTSgPiWzYdmJkcgYh3kt/VUrLsXXC576ogzSjVtNmbrmULotEjef5exiK2o4r3HQ4HocJXJNKcnHcUSYMWhBKCcKIadVajM+Kzw/67vdJRxKL2eK9tGYQCehn+VgCPwmJ17mFkxLFRMNSJIreYIGyOnhAukaIWcFczp7acqmZII9SFItWRU4QiRi0E/yq6QZ0JYMD+jD5R29PErcgE+GeKG7FzBVxjHCrmQDwHkvocmbWKNZWYECZP38+wV5CQ5CRnVUY0TDIh0jj+iaEJlevG1ux9H2RWHKrmQAQ9EHXmYU5m0DTgmym09kCGJwdtIcSQLugCXxFEBaFdMzYTG+JnmcHfef7qKOqOL+VyMFczBRQRpbzd5cG7/yBfKQ/wV32W7rT1YQ1RhIHfd/7qvfYRFHcyp3P0Tydq5WyrcCLQNBQfSFT24KmxJ8txwyiSLq45SBF0PfMjtXEx5ZlVcXamO8pxyhUE3QXVzMMpVDcpLguivOWBTMY1CXyuTGrH5Rkv7yhnBZQ0kaGAmjMGr3EgIK5CaXY/oRFKZuQNnGqq/580Pe875pB30NVrFoVSy/it9gxAy7F3s43iDGjVKmsGGMwz3Vjf/hFQSjifNoUrHRJnAjuYQKzFdjl0XlNpgWYwbwRSFDqRiZ0WcjUEmiCNMINMEHThMhQJsjifDq9MEao7huEfblmPKRYjBVFPLuBXl3QZ9I1GpTfG6WVBOFzc2KyW5HnsM74yzYxE2QddiDGQNDrPkIwOiVI10ecXXqGvpAYvYpLkvogosH+Jh6qVo+EX/G/uEwzz1WSbeXRbF93yVrPXGBRQCkQrM99cxev1DuEyjb0EIOe5MFNCL0AhJNAfmNqQmWXfnbjWOOrdfSIz0Oqk3qoXffOcVz7guJNFc43WFb1PXvDms2AZp55VA9LsCpkAzfgDxICef5GudaXaOb4ex6fqqKX/Kuqz/f82TtvZqN5UcGZmXeOHj36zsyb22JGGmmkkUYaaaSRRhpppJFGGmmkkUYaaaSRRhpppJFGGmmkkUYaaSRX6X8BGXsxrLTqCGkAAAAASUVORK5CYII='
        />
        <a>Serbian</a>
      </Menu.Item>
    </Menu>
  );

  const onLogout = (e) => {
    client.logout();
  };

  const menu2 = (
    <Menu>
      <Menu.Item icon={<LoginOutlined />}>
        <a onClick={(e) => onLogout(e)} href='/'>
          {loading ? 'Loading...' : data[1]}
        </a>
      </Menu.Item>
    </Menu>
  );

  const user = props.user;
  return (
    <div className='header'>
      <img className='imageoflogo' src={logo} alt={'ass'} />
      <div className='translation'>
        <div style={{ marginRight: '2rem' }}>
          <InfoCircleOutlined
            style={{ marginRight: '1rem' }}
            className='infoicon'
          />
          <BellOutlined className='infoicon' />
        </div>
        <div style={{ marginRight: '1rem' }}>
          <Dropdown overlay={menu}>
            <div>
              {language === 'en' ? (
                <div className='translationdiv'>
                  <div>
                    <Avatar
                      size={24}
                      src='https://cdn.britannica.com/79/4479-050-6EF87027/flag-Stars-and-Stripes-May-1-1795.jpg'
                    />
                  </div>
                  <div className='translationtext'>
                    <p style={{ color: '#1C3D6E' }}>English(US)</p>
                  </div>
                  <div>
                    {' '}
                    <DownOutlined
                      style={{ fontSize: '10px', color: '#1C3D6E' }}
                    />
                  </div>{' '}
                </div>
              ) : null}
              {language === 'sr' ? (
                <div className='translationdiv'>
                  <div>
                    <Avatar
                      size={24}
                      src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAARMAAAC3CAMAAAAGjUrGAAAB71BMVEX////GNjwMQHbHNjoAQXkAPHREYosANHEANnHEKTCQp73Ozs3ZtrvQSkzBOEHErq7kra6lCxTGGSO2iIjEmirkqK/Ly8urERr5///HLDPW1tbz8/OsHSPtuS7s7Oz39/f1vy/d3d3JLz3m5ubUpizDniu+v77GIyvHzs2dFhvb29ujQkTGLT3isS3mqq+vr66mTU+lMjW7Izu7kymjAACam5nw09T5xi7Uqyquci3Glizi7u3HsoenIyiMawC9miiuNzufVleyfiylbiyiXS6lUDGUTi6dfyavkCeiJDahPzPRqkyQcybAsrKxDRemp6aEZGTFDxqyd3jRam6/Gz2fYi2yWjGCL0FeUkJKLVRnM0dpY1MALGVOTkgAJmmhMSyaY2SPdnQHNmJXZ38AF2AsHD4AHkOEj5++n6CbhoWhjFucgzywqIu4jQmtm3WdfAXJw7ivcxmpSSihcxvBpmnMnADJuJWNeUV9WgCclYG5lj/mrgCdk33JuJO7tKSfRh2PQUKEYieSPi9zRCiMUhSdRDFuXjRJTVGwYWeNKjGJXCmvYipsVx6nZgt3aB+RPxSUVENpODWkCTgyRFumhzlYWVnX4fMAAAB6e3mJAACFUlKfkZCBGhxpcnBlISBDFBDgh4eIfGDbjpHSbnLMU1hbuXmoAAAU1UlEQVR4nO2djX/TRprHR6d2wasSE1urWLJly5IsEcfClkTiN9w4ToxlE9vETbh22+Wlt3drKCaUEnqFPbakpU0IZQ+6vQuld9fj+ENvZmQ7DmGX+3xQLq7w7wNEHklI880zz/PMi2RAjPSiwEHfwBBqxGSvRkz2asRkr0ZM9mrEZK9GTPZqxGSvRkz2asRkr0ZM9mrEZK9GTPZqxGSv/h+YiPt/CWe1/0xq84l9v4az2ncmRia98AuzlP1mUjTKseXqL8tS9plJMZPLaVl+5RdlKfvM5HyGzmml9PyIyYCKxtmYqI/azqDYv3/f+oAg9/kqzmqfmZC/PfyhfOjj/b2I09pvO/noo99Z5w799hdlKPvDhCRJlVRVVj2fTgu8kC6hbdbj+WU0ov1gwrLFqRSTYqgL+Yvnzn38D78/9I+c0spf8LYnUh52+LE4z8Tj8c/lwTGgAJAEyj/9HoA/nPsdgB+TAJZebE+pw07FWSYk6ZlsXwIRJnA5+cnly9aVh3KHoa6urV6F26vTV65BNFR7Y2K4jcVBJiTLpkLcMRBmmp13K59mt86Wr5+KrQXBSdm8fr18duvTz95d/TyuACk/lyqQw4vFMSYkWWxdAMeSCpfsrJYNEYkQ9dgaxZ2kSwkCFyT0bOeTqMJMA25qbmiNxSkm5FSBAslIBYCTnYHBAT12Ow5uyKV+CaTUCYKr16BzmUp5HLq4w3KGCakWQGA6cvndTlORLWNnR3V2LQBuytmd/o64JV/lFjurjNQE3iXWkcs7LEeYkMV8Mhhe7eRKnwNgVgZ6N9VZoUndogeZnF27Bq515tc6X4Bk3j+MUBxhos6BwD93SnqibEnM2mx1Zw+0E/CCnZTWrka+MBPGQubdO4BaGkKn4gQTTz36x88zNWgehnUreK0zMFqix4QmZ8Z2/AlRtW4Ek2tb0P8apdWTzfzU8EFxgslEANyI6QiEWPoCSGuzfYcilmPITrRKn5KY7ZwEUkdH20auI4EhbD1OMGHnFNpuMGK5c4W70in1EIhZ+jYTvCHsuF199jSn3LJdjljq/LGpOnADDsuRtuOFTtN2GdXZm0C6PXuvuydhybfj0g2hX2CY8kkQWe0eHPsLACEHbsBhOcGE3OD+uBbDzUEsmXeYk3Kv9STmZzsc8y+0WbQ/iwsxmWFu2McmTPqWUh/CHMWZuOOHhkIjDtCB3IGJbD/OiKcqa7cri/2mU5y9ei28ipsO5HMLSEtOXN9hOcKEvRD44nashjbFe7NrfOUUZEL+icSMaJlGDMjrBbi7uGimKzmMCJrJapgZQjNxKI9lvwTMWhVPg+oVk6Y/M4pE4dgFduyuXtK0jL6yToSUFkvotZhp0l+J6EDx08sgOYxm4lRuv3Hz5Oo6KX49SVTnNUGrVVbEDUCpVUurLlbWs7H57sd7OU2L1Yrs1wVirHL55J3hC8SEY33A87FVeqGoMvnQw4WVyt2tjFWeAlQoQ2vWN98uapq8sAHqoZIpr1cqm5V7bZASNy268tlQzvs41S827vH3F0McuGjyi++/X+HpTEhqFgaYrIMLWzFBfvDtA5n+TgJflu/TOWM4530cGz9J3H1YyVwET2O89f77dzOxcs2ki7DtbFYqp0qx+UQ29lnVylQefP+Qj30H4h3r4cOaU9d2WM6Ns+mHH8T+fOfyZiW3aGVrli7mSuLY3fOP+XR2eWVdrKar4sPSZsy6L5/6+tq/VsxvPxhKZ0I4yYR89KBS3pKXT+k5QagZRJXXdFEUy2mTXhbF5RKfFceWaSGt3zfKmWpl8ZuiY5d2WA6OxyZi6+WzfGl5OUPLmr5cyljavcd6Rq6k56vVkrDI60YpXRHKYk2LVWvyvVf/jwckB5mIC5nHOSuWywqmqfEZwZLlNC/wpkYLvIA+8Zm0afJyVlvkqysZ49X/4wHJybmMxDwvmJqpybJA07TJCzJM32hB0OwfMi6z+Bgt0HxmaFuOw/M7RgVWWpBlWPu/Jlmj0SGx9aHMTGw5yiQxj6stazAheQkXvEPDO2Z1J6/rsBxlMoYaCW1VYO0tUxOE01egSZgWtIzTNwVBiFkmBLKIjjHPvil2QhQtWhZuXhPoxXLtPi2fvimb3xw69L1Jn77Jm5VybcHS6H+7zdPmUC/mcpQJ+cGhj03a7JiaATOSjGyuVdIfH377nKlVbi+moWmI2YzZoeUPD71dcPK6DstRJuzHb537i0zfoumFanXFlE8rAl358IdvaO1GUjAruqGbtAWDz0eH39ocvuH6vpy1k83vPzd52Dqg65iN8Tx9FbpUy6I17fRlntdmrVktffNOGnL6YIiROL3WolaBDrSzE2dwpEFRqB+HOnCveVccaibka2psl07B5ENIWxWo2B6hYguGI4G3jF1nEa97E84KvPVaOvzro7t0/DdYx6F+s1f90uO7z4ofer27cFjg715Lh39NeF4in8+nvqRYfXmx751Dr3cXDuu1mRzpeQaS9XjwtmgU215/qvHCujVW1Vttb6s7tsYOLPbzuJUJW2i122MkkdBPaZ8AqEB9Su3Xm1Qn/HFUem2tUoM9Yrblbad60NzKpNEOwBrPkeJ8WqPT000OAaBahQk0mNYYT3kj8HNQadK8KcA+sRpFu3XWzUwaDGIgNQgjw8MYLDf/XZmWsLVcYonJPOIFlOQPP1wRTIFOl0XWj3emWPcy8SmoiqDFEomSwPO8cCOpBGFBGIA2ZAJABH1SlOZtHqkKIeYxFLz6xJVM7N86iDdgBasZVGvhCi6RqIshtP4viZsSCK8JmsALaM0O2wJdy3InE1IP9MwERp0aLz8+lb5N4SLA+ND6FHs7YKbNxwtpe9GSbSigrbqTiVq361xADaFYLJdF8bHeCduFKZLw2VYCTn732CCNeWMTMemeFNBJNzIhdTDARLxbNgx9K0dftgvrKjllb3GruWy5alQ/Oz/ABHhVNzLxtLtMNnAUSWStDA97fGt26wmMe7z27ls0HdO0WRMjIXzxboOacCMT2zMEkUng2ooJY5HPmLk7XVK+KP4ZWc3F+Mxmwh5hQ8YT7noh9zFhU7hqMBoHCr20daxqJNiu+bTH7Z/BCTFRrfaOQE0njP2MorqPiYrrzti+Y6B/03MjzFzPsezZydiNR3UfE65fPTA3OC0+bkdoyU5edi+EbdjtCZ+Vch8TXHUbCYgUBvrCDckunAZdv7GzS+26XXya331jBfqOmcB42xhgEgE9T/OCnbAXuodHkI3Vl1zHBCXp0W6CBivo61e8BXZpBxebCvQKEUvKdUwIv101iaGwQVzqulKyENnNZGcP7gqEkxQ8C9lJ/oTbmJB1mwn8k4wO+NkGB1MWWAAzt7AEN8I7e3C2pnDYulBi5z4mEAYVRF4jHOaCPT9LNhSEKRxGT9BSEqw6g0YG2L5/VRCNpO1sdNcxgUYwjW0kHrHdKXQcZBE2imgTMExYiQDYSKYjUQggAGOP7V8hRchkGp4BT9lwG5Ningkz4WkugirJYChKQz0T56hgkwEcB5igFEkCjmI4iktOqjiDC0/jVsMlqXCU4fxuYzJ2Cf6+JS4awU4lgjt+zeljSAr8yyjNbUbpfTqmbGPPy1DYAVGQGAOCT9zGhGyj6lHBKMrNoGEgl3Im1FPjaViSpPCTfsGTeM+ZNKFlYTQB9/lYL2YC/05jp7mNmLB4AoclewnrOJr+QTMb5CQMRZwCpDjgcMtC5uI+JnUQ5CATSolGwpBJUsFMPC1vu1Vge0zIiVTb24YRCTKJSkkQbAIKel0pjJiMu5BJOA6ZwCSEYzggKbBlnGFhEktFUVemy2QiDwLQ2arEpBRFrQw6IGRa8CS3MonaTJqoCUEsyTOkSv04teQfYBKIjod+knSykGwCfGAUGRf0Qi5nAitKSbAVSWdYjzcKJk4EBpi0fPVwIEROStMgDhQUiN3PJIL+wRGFiUN/EvK21V12IoVSDEzuJ+NM3O4JuJmJFwSRP4mgOuLkFFDIx6am1PogE6B7NiYIotAEcfs4zjYuV8adbixGoadbV/CIhD42MDEx2HbacDPqIQpJfESXiRR0JROi/QITBsVi6GO9Pn3QTrxLG62ojvOTHTvB+UnEdUyO+F9ggvMTj1dq+QOgPdB2lFYb+di4ndWDSNw+gwGS68aUjqT6TBQMZBq1HTLEBPKMv9jP2Ta80UD+AvSxCoCZHfRAUjSIIhX0z4r7mEwinxBH3eJpBASm7NKPKsv6Gks+le3ayQlW9S01lmDx5DQaOoAdHYqKRuxhlUvuG6NOHbMjK6dE4kqEo8ISte3vyx683vl8BobiCEVxQQb2ejCT4FMXMglyMA4zQIl0x82avdmLl4mLx6NoRAn1jAA+j2Hc13amoIUk4W+82Qso1N9iQnEwuYsGAXK1YRiMk0k3zmXoMMuINJWkAvrB+G8ygc0Mdoe5CPQ8TQrlvS5kMom7xDtG8komFBq6xidEcDhuu8+fjAcAtwPkVW0njtPXcPdQzMSFc6ONfD9d6/05M9WTbq9G2uwXbKKuYjQ4yKTlPiYqZTPBKYrdds6wJNvQCw11Z5xN9Y3rkyqJcntm51A8wODCdQWw+4uZBKODTKbyAOSVVI+Jp43Gpp96CJjbM4MmBalMuo+Jp20zsbt0eHYP9gHr9clUfNdYgX9yLlBAdqIMMqFA3oVrctDQK2oNvbE2cBFcZFXmUrQxPjhW4F/y18OwXxyM13cz4Vy4doucwvlGv47gCfcDS6YC+cbUoJ3UfX5Q95A/KX6vfaQUto+vu5EJDMbUwO89cKJ+LESwJxoqs6vtTKlPfAT5Ixhv9+nZ6YkL1z2SqrSLiXICBJ+QRYlR5waZUL5WwM+y06ANOwPdkUoYkzlXrgUlVcZmgpuPEi9uAHDGpyQDu8ft/UteKliALpZbUp4CPHaCMpUI0EkXMukFHgllYvUGWvm63WD8T8Z3jVFH5k78FCz8GAWg4NH7mSxsauNuXEfN+u0wjKNOm51A7uVRCgQgpQu9/CSkBGCBcoJj0GK/8QEmkivX27MbqKsbtEdYW2gJsQKiT/TWxvmdPJZQx1OtuaVtEGRAuKFKYanHhHIlEzIFmcBK4q5goQjCTRhl/yOElhUMrCtAr79/BJPZJgW8S3XMz3bJLmTynIQJCheBTGA9OWrpIhfEi5C2QySxiwnB/mcQLQQMUsfGN3qRCqUnhMvy2Lf/8J79/E44jDo88dRSFK2jQKMF24VddkKy/4VXtiGPMrdE9YYXgNdDqPUhY/L2a+nwr35mMROY3EeD0pc+NNvDcWgBUmr7kYfs94vZJ9uT0A8rDAOPlBqhp7uZvN5dOCzwq9fUcQ9mgvvDbV8KPYHCoVSM84UCyUcnbCZPftrO+9WWPQmURMvJCz0mqO0or3sTzuqvj4j9H3XUQ6ZsJl4llIJx+IFdW5iz+kE8OT09vQ3/HkNPiMIojdK7O1fCoO5rn7FXUddVcuy1b2LINKPCWIznO1sqWax21v4bVTUJpkjIoImWEiswG4lG/SyhUhzuBrx7+88FwoMeD0PzxSr7PwddB8e1hJ4upuLowT5CXNCsk+AS7AcqIRhoLoWjkTgTjTCAOaaTBNue8oI6F1+roJf4occIn8I+j+o5ftBVcFzPYZYW0L3ogXOimqmUqsVG4AKLXtHu48JJsB0G05SkoGdqiQI5IfmXUpuz8opIkIUARAdze3XmoKvguJ4tcVKKTTENgkiUKqX1+a2a/W4G6GeoqIJ6QRSDczNUFqpuLWTXLfQ2d/ZCUm14Qcp30DVwXjPqlFrVqx4xYSykF1cEM1OyX5brY9CMKWhGkkCBuYv9xRji3Rxvyl9pdDWREI3zVU+h+N5B12Af5BEX0paWy+VogU9ncpnN7tOyczDhD0oRhUH5GeTTfdJ2eTFjCoKglXI5eZafNdSjB12BfdAzVq9auVJJFvhM2Uj03oGqIjPh7JESnMn3vkElYSSqJYEXSqVSbL1a9L347JMbNOMjyOq9rZxgCrpIiPe7L96esOfC8KgAXgLYfSBQLENfsmxmMlq2XEsQ7M8Hff/7oudkomKm5U3ZTMAa977W2qNgO8EjTSgPiWzYdmJkcgYh3kt/VUrLsXXC576ogzSjVtNmbrmULotEjef5exiK2o4r3HQ4HocJXJNKcnHcUSYMWhBKCcKIadVajM+Kzw/67vdJRxKL2eK9tGYQCehn+VgCPwmJ17mFkxLFRMNSJIreYIGyOnhAukaIWcFczp7acqmZII9SFItWRU4QiRi0E/yq6QZ0JYMD+jD5R29PErcgE+GeKG7FzBVxjHCrmQDwHkvocmbWKNZWYECZP38+wV5CQ5CRnVUY0TDIh0jj+iaEJlevG1ux9H2RWHKrmQAQ9EHXmYU5m0DTgmym09kCGJwdtIcSQLugCXxFEBaFdMzYTG+JnmcHfef7qKOqOL+VyMFczBRQRpbzd5cG7/yBfKQ/wV32W7rT1YQ1RhIHfd/7qvfYRFHcyp3P0Tydq5WyrcCLQNBQfSFT24KmxJ8txwyiSLq45SBF0PfMjtXEx5ZlVcXamO8pxyhUE3QXVzMMpVDcpLguivOWBTMY1CXyuTGrH5Rkv7yhnBZQ0kaGAmjMGr3EgIK5CaXY/oRFKZuQNnGqq/580Pe875pB30NVrFoVSy/it9gxAy7F3s43iDGjVKmsGGMwz3Vjf/hFQSjifNoUrHRJnAjuYQKzFdjl0XlNpgWYwbwRSFDqRiZ0WcjUEmiCNMINMEHThMhQJsjifDq9MEao7huEfblmPKRYjBVFPLuBXl3QZ9I1GpTfG6WVBOFzc2KyW5HnsM74yzYxE2QddiDGQNDrPkIwOiVI10ecXXqGvpAYvYpLkvogosH+Jh6qVo+EX/G/uEwzz1WSbeXRbF93yVrPXGBRQCkQrM99cxev1DuEyjb0EIOe5MFNCL0AhJNAfmNqQmWXfnbjWOOrdfSIz0Oqk3qoXffOcVz7guJNFc43WFb1PXvDms2AZp55VA9LsCpkAzfgDxICef5GudaXaOb4ex6fqqKX/Kuqz/f82TtvZqN5UcGZmXeOHj36zsyb22JGGmmkkUYaaaSRRhpppJFGGmmkkUYaaaSRRhpppJFGGmmkkUYaaSRX6X8BGXsxrLTqCGkAAAAASUVORK5CYII='
                    />
                  </div>
                  <div className='translationtext'>
                    <p style={{ color: '#1C3D6E' }}>Serbian</p>
                  </div>
                  <div>
                    {' '}
                    <DownOutlined
                      style={{ fontSize: '10px', color: '#1C3D6E' }}
                    />
                  </div>{' '}
                </div>
              ) : null}
              {language === 'ur' ? (
                <div className='translationdiv'>
                  <div>
                    <Avatar
                      size={24}
                      src='https://upload.wikimedia.org/wikipedia/commons/thumb/3/32/Flag_of_Pakistan.svg/1280px-Flag_of_Pakistan.svg.png'
                    />
                  </div>
                  <div className='translationtext'>
                    <p style={{ color: '#1C3D6E' }}>Urdu</p>
                  </div>
                  <div>
                    {' '}
                    <DownOutlined
                      style={{ fontSize: '10px', color: '#1C3D6E' }}
                    />
                  </div>{' '}
                </div>
              ) : null}
              {language === 'hi' ? (
                <div className='translationdiv'>
                  <div>
                    <Avatar
                      size={24}
                      src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAARMAAAC3CAMAAAAGjUrGAAAAmVBMVEX/mTMSiAf/////lycAgQAAAIgAAIUAAH4AAIIAAIMAAHwAAHPi4u+kpMvz8/kAAHjn5/Lt7fXd3ey5uddPT6GZmcWDg7n6+v7Ly+HIyOJERJxlZao+PponJ5L29vy0tNbW1uggIJCfn8iGhrsYGJBNTaB2drQQEIx+fripqc5ra62QkMBXV6QiIpFfX6i+vtwyMpw6Opg1NZe0gadDAAAEvUlEQVR4nO3ba3OiSBiG4UzvdDcip8YDiAdQZAIxExP//4/btzHjZH1Nze6HpVPlc1Ul0eiH9g7Q2JKHBwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACA/+gvuPYg4BqacGjCoQmHJhyacF+hSTI5LFd7uV8tD5PE9WDEF2gSHNbaGyklpVRq5Omfh8D1kBw3qaZ6pCiGVFrr/oYc6SZ3OyinTZIpdZCeevnhNyY1jf/jRXm0vejG6S7kssnObhmeNxMbcTSiEuZIt2aeZ7eancNxOWzy7NFrn3cnI/p9pbTfcmFO3ZxaeVN3A3PWJNrS5lAEmb3d5wgvt7KgoMe2kauhuWoSPdK2QHvIwt5ZpPTtQF/p+S7tVbQNPbqK4qhJRluJ17VGZB3di2yPF/o62AxdJkzbUZRt5mZwjpo0lOTY3wrtFGMPHq/vP5N+JxJHiuLomOKmycyTXinOUewLX1IYSTmW7/cpiSjpOTMno3PSJNA049TnQ4gIae8J6cVrSkWbSNdvJvRITbOPdnJO66RJo1QzM3RjaY8YMhLVVmR+JraViGhzEZn9vZnZp7kYnosmCy31+UR180oH1a4QWWyiODJxJhqajaPTpn80oee5OM130WSlVL3pZ11hJO0daiLWITUJ12KiaM+Spn9ssamVKhyMz0GTVNsDrOi6/o5fiVKLdmVis2qFLkXl27OV/rhCh1mdDj9AB01qmoftnz+Z2ir5+Cjk08JP49RfPElxHNvdpZvao2vhyVE9/AAdNHmkOfZ8ihpK2l4m4y4fJ7ryKp2M8248oe1D7vrHDc3Z++EHOHwTmojlr9tRISciHB/beZ3v87puj+OdmMiV+fUE6WI6Hr5J6amnaFEuzptK6av8GAfrKmzDah3Ex/zR798Iik1e5tGT8srBRzh8k7lSLf1ID9NlWNF5yHLcBuugLd6KNl0H87E9N0m7p+Zgj66tUvPBRzh8k2cl/fNZRzLbxut2kczXaWFXIEdFsG6TfH6KH2fnPSb3pXoefITDN3lVanpZBTDhytd12o6kNWrTg/ZPu8vKYzRV6nXwEQ7fRErvkiSLIpNWeViovokqdnmVmii6LBJE3u8D8mCcNEmqYzhbnqT2/TiOvX37Js/e6q1Hv4l9LX8+H8IyT++mCbaTK1fHkzccTz7OO8Hveafp550mWM/vct6Zn085cH7ywe3z2JTOY9O7PY/F+50b9vS++PyiP3tfPLm398WiHv15/aTs109Wd7N+gnW2G4p/uR6b3c96rMiv1u2bT9btgztat+8/3zmY989xbn6+I+7t8x18DngLPi++YYrrChhcf3IDrlO6IdqOpCpSXM/2D/11j234yXWPwy+bXOD6WO5rXkc9vdvrqAWut78N/5dx08f/3zF/fvr/7is0+WrQhEMTDk04NOHQhHv4DtcevsE1NOHQhEMTDk04NOHQhEMTDk04NOHQhEMTDk04NOHQhEMTDk04NOHQhEMTDk04NOHQhEMTDk04NOHQhEMTDk04NOHQhEMTDk04NOHQhEMTDk04NOHQhEMTDk04NOHQhEMTDk04NOHQhEMTDk04NOHQhEMTDk04NOHQhEMTDk04NOHQhEMT7m/cNifkUwM0UQAAAABJRU5ErkJggg=='
                    />
                  </div>
                  <div className='translationtext'>
                    <p style={{ color: '#1C3D6E' }}>Hindi</p>
                  </div>
                  <div>
                    {' '}
                    <DownOutlined
                      style={{ fontSize: '10px', color: '#1C3D6E' }}
                    />
                  </div>{' '}
                </div>
              ) : null}
              {language === 'it' ? (
                <div className='translationdiv'>
                  <div>
                    <Avatar
                      size={24}
                      src='https://cdn.britannica.com/59/1759-004-F4175463/Flag-Italy.jpg'
                    />
                  </div>
                  <div className='translationtext'>
                    <p style={{ color: '#1C3D6E' }}>Italian</p>
                  </div>
                  <div>
                    {' '}
                    <DownOutlined
                      style={{ fontSize: '10px', color: '#1C3D6E' }}
                    />
                  </div>{' '}
                </div>
              ) : null}
              {language === 'pt' ? (
                <div className='translationdiv'>
                  <div>
                    <Avatar
                      size={24}
                      src='https://cdn.britannica.com/88/3588-004-E0E45339/Flag-Portugal.jpg'
                    />
                  </div>
                  <div className='translationtext'>
                    <p style={{ color: '#1C3D6E' }}>Portuguese</p>
                  </div>
                  <div>
                    {' '}
                    <DownOutlined
                      style={{ fontSize: '10px', color: '#1C3D6E' }}
                    />
                  </div>{' '}
                </div>
              ) : null}
              {language === 'ar' ? (
                <div className='translationdiv'>
                  <div>
                    <Avatar
                      size={24}
                      src='https://cdn.britannica.com/79/5779-004-DC479508/Flag-Saudi-Arabia.jpg'
                    />
                  </div>
                  <div className='translationtext'>
                    <p style={{ color: '#1C3D6E' }}>Arabic</p>
                  </div>
                  <div>
                    {' '}
                    <DownOutlined
                      style={{ fontSize: '10px', color: '#1C3D6E' }}
                    />
                  </div>{' '}
                </div>
              ) : null}
              {language === 'de' ? (
                <div className='translationdiv'>
                  <div>
                    <Avatar
                      size={24}
                      src='https://upload.wikimedia.org/wikipedia/en/thumb/b/ba/Flag_of_Germany.svg/1200px-Flag_of_Germany.svg.png'
                    />
                  </div>
                  <div className='translationtext'>
                    <p style={{ color: '#1C3D6E' }}>German</p>
                  </div>
                  <div>
                    {' '}
                    <DownOutlined
                      style={{ fontSize: '10px', color: '#1C3D6E' }}
                    />
                  </div>{' '}
                </div>
              ) : null}
              {language === 'ru' ? (
                <div className='translationdiv'>
                  <div>
                    <Avatar
                      size={24}
                      src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAARIAAAC4CAMAAAAYGZMtAAAAM1BMVEX///8AOKXVKx7q6urnKgDWLyMAKqEAOKdQNpKTn84HPKba2trx8OyHlcUAMaQAPanoLQmtwTivAAABg0lEQVR4nO3Qy1XDUBTAwBtMCDjf/quFJc8qABYzFehorhuL62zDYrPkyJKwJCwJS8KSsCQsCUvCkrAkLAlLwpKwJCwJS8KSsCQsCUvCkrAkLAlLwpKwJCwJS8KSsCQsCUvCkrAkLAlLwpKwJCwJS8KSsCQsCUvCkrAkLAlLwpKwJCwJS8KSsCQsCUvCktjm9tcJ/81t7l8s7vP4YPGY/cRit+TIkrAkLAlLwpKwJCwJS8KSsCQsCUvCkrAkLAlLwpKwJCwJS8KSsCQsCUvCkrAkLAlLwpKwJCwJS8KSsCQsCUvCkrAkLAlLwpKwJCwJS8KSsCQsCUvCkrAkLAlLwpKwJCwJS2Kf5+nCL6fnfHIwr3cWrzm/sThbcmRJWBKWhCVhSVgSloQlYUlYEpaEJWFJWBKWhCVhSVgSloQlYUlYEpaEJWFJWBKWhCVhSVgSloQlYUlYEpaEJWFJWBKWhCVhSVgSloQlYUlYEpaEJWFJWBKWhCVhSVgSloQlYUn8LOHgG0Xm8InP9LwRAAAAAElFTkSuQmCC'
                    />
                  </div>
                  <div className='translationtext'>
                    <p style={{ color: '#1C3D6E' }}>Russian</p>
                  </div>
                  <div>
                    {' '}
                    <DownOutlined
                      style={{ fontSize: '10px', color: '#1C3D6E' }}
                    />
                  </div>{' '}
                </div>
              ) : null}
            </div>
          </Dropdown>
        </div>
        <div className='avatarimg'>
          <Avatar size={36} src={user?.imageLink} />
          <div className='namediv'>
            <p style={{ fontSize: '12px', color: '#1C3D6E' }}>
              {loading ? 'Loading...' : data[0]}
            </p>
            <p
              style={{ fontSize: '16px', fontWeight: 'bold', color: '#1C3D6E' }}
            >
              {user?.userName.slice(0, 7).toUpperCase()}
            </p>
          </div>

          <Dropdown overlay={menu2}>
            <a onClick={(e) => e.preventDefault()}>
              <DownOutlined
                style={{
                  fontSize: '10px',
                  marginLeft: '10px',
                  color: '#1C3D6E',
                }}
              />
            </a>
          </Dropdown>
        </div>
      </div>
    </div>
  );
}

export default Header;
