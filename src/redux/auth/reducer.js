import * as t from '../actionsTypes';

const initState = {
  counter: 0,
  isLoggedIn: false,
  isLoaded: false,
  pinSelected: false,
  userProfile: null,
  lnglat: null,
  setLanguage: 'en',
};

export default function authReducer(state = initState, action) {
  switch (action.type) {
    case 'INCREMENT_COUNTER':
      return {
        counter: state.counter++,
      };
    case t.SET_LOGIN_STATE:
      return {
        ...state,
        isLoggedIn: action.payload, // we set this as true on login
      };
    case t.SET_LOADED:
      return {
        ...state,
        isLoaded: action.payload, // we set this as true on loading
      };
    case t.SET_PINSELECT:
      return {
        ...state,
        pinSelected: action.payload, // we set this as true on pin selected
      };
    case t.SET_USERPROFILE:
      return {
        ...state,
        userProfile: action.payload, // we set this as true on pin selected
      };
    case t.SET_LATLANG:
      return {
        ...state,
        lnglat: action.payload, // we set this as true on pin selected
      };
    case t.SET_LANGUAGE:
      return {
        ...state,
        setLanguage: action.payload, // we set this as true on pin selected
      };
    default:
      return state;
  }
}
