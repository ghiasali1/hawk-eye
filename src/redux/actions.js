import * as t from './actionsTypes';

export const setLoginState = (payload) => (dispatch) => {
  dispatch({
    type: t.SET_LOGIN_STATE,
    payload,
  });
};

export const setIsLoaded = (payload) => (dispatch) => {
  dispatch({
    type: t.SET_LOADED,
    payload,
  });
};

export const setPinSelected = (payload) => (dispatch) => {
  dispatch({
    type: t.SET_PINSELECT,
    payload,
  });
};

export const setUserProfile = (payload) => (dispatch) => {
  dispatch({
    type: t.SET_USERPROFILE,
    payload,
  });
};

export const setLatLag = (payload) => (dispatch) => {
  dispatch({
    type: t.SET_LATLANG,
    payload,
  });
};

export const setLanguage = (payload) => (dispatch) => {
  dispatch({
    type: t.SET_LANGUAGE,
    payload,
  });
};
