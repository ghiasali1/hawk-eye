import React, { useEffect, useState } from 'react';

import './App.css';
import Login from './components/Login/Login';
import Signup from './components/Signup/Signup';
import App1 from './components/sider/Sider';
import { client } from './config';
import { useDispatch } from 'react-redux';
import { setUserProfile, setLoginState } from './redux/actions';

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from 'react-router-dom';
import { setConfig } from 'react-google-translate';

setConfig({
  clientEmail:
    process.env.REACT_APP_GCP_CLIENT_EMAIL ??
    'hawkeye@hawkeye-328515.iam.gserviceaccount.com',
  privateKey:
    process.env.REACT_APP_GCP_PRIVATE_KEY ??
    '-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDWLKFCDRzZIC2q\nAJAsIqCyx9NqibJWY/lFXq6dLN4DEj09+HjYOth2hfpH0bA8xz/l8sUKHqMvMnsY\n2TOJZfGqShCR+0poX0a4fTxxwUCjLw1C4c+rbJfsnCybX+cFqljZuvoDfB2P1q2r\nLoOyTXKBT9/Zm+iui5cbAAvLkJqEr1jtYUA3xd9CxKIUGuQaMxBDh6U1iDRX32it\nkm8wMvNVTzpyehlRfKYmSWCr82xCUaOzj3bn7+zi8nt0T+UodlzUfqPFbbyzflzQ\nsa2MmNFvBe2DtCYYTnuDpEsXsQqPhvxvnT4gMA89lkNWD2z513mEISTjGnGg14e2\njKyPv4l1AgMBAAECggEAIj30ckkCabx5UIrSghvvMWY9aPUm7CUDv5GJvVtlC3yp\nCfVl7LxLy3dUMpNV6AG7N5p+4kVPtcYOZESltrvzzVQWHiDdHp7NAcYUGePZ370w\nE5diyneMOqolf5nvp7o6hmUcf94iI9Mg5ROEvscfHYmVsob4xp9Ow2RYfm1aJwhO\n/dhN0zpNuGM0VLKg6bOxWWDlPOuUiqBbq/ewb/MbAhZuUrmjmzhPYDyZTRg/U2++\nefpi+NGJkg0b35rpPEfI/NSXfKawg9FJylwwQEbPcVTSGel9mW64bvFCyO4PO5wx\nhb8xMOiNf4OAD/Li3OC9gLBzm6u6zA8qnWfnozw5KQKBgQDu4C//zRrelcTcAwy/\nmEjR8S6X+vAwQG/vG95fPl3jwAtzU5apCWEhBTAyPcZONhIk8zJflycXUKnReBfx\nCsvPidukGi6clZ8D/LfUn3Snt46FjUO5PzSh4XjkUqaSoFXPKYFfevdPpg57Jrbk\nN54kWoRdiM+uAOa6rO6xga6plwKBgQDlhyBBQkEKrRMCOJAhkUrfDxrIlD1iyRWC\nKUvGqDR4LB9dJ5ZSmZ18BvTi8UpHcZEAvLoB+zXwT2vAGnJG6WfFtvqnSrw9CC53\nxIkNvL1b/ps8zC54aAVtmk9Sx4+rJvXT849DpTpZ2tdLZiX9HD3eJKEe0RjpQWOd\nk4xdMgiO0wKBgQCq/U9ejDLvocWIt6Yof5mgrkPsBnRoFbgKMKi9BKewn4eSxgLS\nfSRJvq0BSbbpxUq3qF4mjswspFDq4ExC/rdlmAjhLtyuuWtaL0xnpryaUYpzjusr\nb1L9jag7x+30h1DlOZCiCCN/oB49iTjho+5Pl3uoeIYwx/RhK+iwp2OfgQKBgFam\nRYn9XhqOrh9nhRn3BMHirOTN+cKiUQlbV6z+cRhw58I+IKS8M1TQN4C5QKZp/CjA\nFqSK1pkLEo1qLnwQv0lHw6aGMzwDK6+xNWBHQWFKZAZ2upBgDLSMzaddiSI+yg0i\n5JHP/ZBQanifbnX4RG7pex3sKDBerCGfwxynt81bAoGBAM7OCw4A36khsr6PSHpS\nAFrhEZ3xREC4/diLEJDTO0zMF37IbxCNsw7/jlJRFgolRnYsD+dGrpNaiVV6q8WK\nMPLiEDImg8POE966+/EoE/V5n8hOyWsYTm+g1UEFLp7Tz/5v4D4LoLkPfuKzTi20\n4PHDq5JzcCWbl8hx7jjRk2Hz\n-----END PRIVATE KEY-----\n',
  projectId: process.env.REACT_APP_GCP_PROJECT_ID ?? 'hawkeye-328515',
});
function App(props) {
  const [loading, setloading] = useState(false);
  const [isLoggedIn, setisLoggedIn] = useState(false);

  const dispatch = useDispatch();

  useEffect(() => {
    setloading(true);
    client
      .reAuthenticate()
      .then((res) => {
        setloading(false);
        setisLoggedIn(true);
        dispatch(setLoginState(true));
        dispatch(setUserProfile(res.user));

        props.history.push('/app');
      })
      .catch((e) => {
        setloading(false);
      });
  }, []);

  return (
    <div>
      {!loading ? (
        <Router>
          {isLoggedIn ? <Redirect from='/' to='/app' /> : null}
          <Switch>
            <Route path='/app'>
              <App1 />
            </Route>
            <Route path='/signup'>
              <Signup />
            </Route>
            <Route path='/'>
              <Login />
            </Route>
          </Switch>
        </Router>
      ) : (
        <h1>Lodaing</h1>
      )}
    </div>
  );
}

export default App;
